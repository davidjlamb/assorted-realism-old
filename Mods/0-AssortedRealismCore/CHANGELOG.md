# Version 3.0.15 (2020-05-14)
## Features
* Increased tire durability to 500
* Fixed smelting of empty cans
* Fixed smelting of metal parts
* Fixed smelter having clay and stone recipes
* Add loot lists to open objects (cabinets/fridge/etc)
