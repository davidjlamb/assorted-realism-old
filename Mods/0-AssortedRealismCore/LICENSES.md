Cardboard Box
    Source: https://pixabay.com/vectors/box-cardboard-cube-isometric-1299001/
    License: https://pixabay.com/service/license/

Book Isolated Cover (black)
    Source: https://pixabay.com/illustrations/book-isolated-book-cover-empty-3057902/
    License: https://pixabay.com/service/license/

Book Isolated Cover (red)
    Source: https://pixabay.com/photos/book-isolated-book-cover-empty-3088777/
    License: https://pixabay.com/service/license/

Branch Stick
    Source: https://pixabay.com/vectors/branch-stick-tree-wood-1295738/
    License: https://pixabay.com/service/license/

Ingot
    Source: https://www.cgtrader.com/3d-models/various/various-models/gold-bar-1c6d48c6-a818-41e1-9cc4-ef6a35a626d9
    License: 

Suspended Cooking Pot
    Source: https://www.cgtrader.com/3d-models/household/kitchenware/suspended-cooking-pot
    License: 

Medieval Bellows
    Source: https://www.cgtrader.com/3d-models/various/various-models/medieval-bellows
    License: 

Wood Stove
    Source: https://www.cgtrader.com/3d-models/furniture/appliance/wood-stove-71fb3d06-16cc-4c78-9507-b4873dd94cde
    License: 

Copper Ore
    Source: https://7daystodie.com/forums/showthread.php?131108
    License: 

Copper Boulder
    Source: https://7daystodie.com/forums/showthread.php?131108
    License: 

Zinc Ore
    Source: https://7daystodie.com/forums/showthread.php?131108
    License: 

Zinc Boulder
    Source: https://7daystodie.com/forums/showthread.php?131108
    License: 

Primitive Engineering Workbench Blueprint
    Source: https://pixabay.com/illustrations/blueprint-vintage-sketch-futuristic-3194878/
    License: https://pixabay.com/service/license/

Mortal and Pestle
    Source: https://www.cgtrader.com/3d-models/science/medical/mortar-and-pestle-8adc3251-75a6-4ac4-aebf-b750e12fa978
    License: 
    
Blacksmith Tool Set
    Source: https://www.cgtrader.com/free-3d-models/various/various-models/blacksmith-tools-set
    License: 

ToolsPack
    Source: https://www.cgtrader.com/free-3d-models/industrial/tool/toolspack
    License:

Hacksaw Wood
    Source: https://www.cgtrader.com/3d-models/industrial/tool/hacksaw-wood
    License:

Log - Pine decks
    Source: https://www.cgtrader.com/3d-models/architectural/decoration/log-pine-decks
    License:
    
Sea Trout
    Source: https://pixabay.com/photos/sea-trout-seafood-salmon-fillet-1798847/
    License: https://pixabay.com/service/license/

Fish Salmon Filet
    Source: https://pixabay.com/photos/fish-salmon-fillet-omega-3-omega-6-2631412/
    License: https://pixabay.com/service/license/

Pork Neck Steak Meat
    Source: https://pixabay.com/photos/fry-pork-neck-steak-meat-barbecue-5991/
    License: https://pixabay.com/service/license/

Beef Ribeye Steak Food Meat
    Source: https://pixabay.com/photos/beef-ribeye-steak-food-meat-1814638/
    License: https://pixabay.com/service/license/

Chicken
    Source: https://pixabay.com/photos/chicken-broiler-meat-1140/
    License: https://pixabay.com/service/license/

Brown spool of rope
    Source: https://www.pexels.com/photo/brown-spool-of-rope-on-gray-concrete-floor-3650426/
    License: https://www.pexels.com/license/

Primitive Fishing Rod
    Source: https://www.cgtrader.com/3d-models/sports/equipment/old-394c0c06-b872-4165-bc28-9d57218a6079
    License: 

Fishing Rod Woosh
    Source: https://www.pond5.com/sound-effects/item/8747913-fishing-rod
    License: Individual License, Commerical

Tree End - Tree Annual Rings
    Source: https://pixabay.com/photos/texture-tree-annual-rings-2119287/
    License: https://pixabay.com/service/license/

https://pixabay.com/photos/grid-paper-paper-school-science-1806470/