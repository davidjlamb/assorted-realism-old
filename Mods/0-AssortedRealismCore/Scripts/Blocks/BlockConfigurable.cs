using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using GUI_2;

public class BlockConfigurable : Block
{
    private BlockActivationCommand[] cmds;
    private string[] buildableList;
    private float configureDelay = 5f;
    public override void Init()
    {
        base.Init();
        if (this.Properties.Values.ContainsKey("ConfigureBlocks")) {
            buildableList = this.Properties.Values["ConfigureBlocks"].Split(',');
            this.cmds = new BlockActivationCommand[this.buildableList.Length + 1];
            this.cmds[0] = new BlockActivationCommand("take", "hand", true);
            for (var i = 0; i < this.buildableList.Length; i++)
                this.cmds[i+1] = new BlockActivationCommand(this.buildableList[i], this.buildableList[i], true);
        } else {
            this.cmds = new BlockActivationCommand[1] {
                new BlockActivationCommand("take", "hand", true)
            };
        }
        if (this.Properties.Values.ContainsKey("ConfigureDelay"))
            this.configureDelay = StringParsers.ParseFloat(this.Properties.Values["ConfigureDelay"], 0, -1, NumberStyles.Any);
    }
    public override BlockActivationCommand[] GetBlockActivationCommands(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _entityFocusing)
    {
        //TileEntitySecureLootContainer tileEntity = _world.GetTileEntity(_clrIdx, _blockPos) as TileEntitySecureLootContainer;
        //if (tileEntity == null)
        //    return new BlockActivationCommand[0];
        //string _steamID = GamePrefs.GetString(EnumGamePrefs.PlayerId);
        //PersistentPlayerData playerData = _world.GetGameManager().GetPersistentPlayerList().GetPlayerData(tileEntity.GetOwner());
        //bool flag = !tileEntity.IsOwner(_steamID) && (playerData != null && playerData.ACL != null && playerData.ACL.Contains(_steamID));
        //this.cmds[1].enabled = !tileEntity.IsLocked() && playerData == null;
        return this.cmds;
    }
    public override string GetActivationText(global::WorldBase _world, global::BlockValue _blockValue, int _clrIdx, global::Vector3i _blockPos, global::EntityAlive _entityFocusing)
    {
        string str1 = Localization.Get(Block.list[_blockValue.type].GetBlockName(), "");
        PlayerActionsLocal playerInput = ((EntityPlayerLocal) _entityFocusing).playerInput;
        string str2 = playerInput.Activate.GetBindingXuiMarkupString(XUiUtils.EmptyBindingStyle.EmptyString, XUiUtils.DisplayStyle.Plain) + playerInput.PermanentActions.Activate.GetBindingXuiMarkupString(XUiUtils.EmptyBindingStyle.EmptyString, XUiUtils.DisplayStyle.Plain);
        return string.Format(Localization.Get("useWorkstation", ""), (object) str2, (object) str1);
    }
    public override bool OnBlockActivated(int _indexInBlockActivationCommands, WorldBase _world, int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
    {
        if (_indexInBlockActivationCommands == 0)
            return this.OnBlockActivated(_world, _cIdx, _blockPos, _blockValue, _player);
        LocalPlayerUI playerUi = (_player as EntityPlayerLocal).PlayerUI;
        List<Recipe> recipes = CraftingManager.GetRecipes(this.buildableList[_indexInBlockActivationCommands - 1]);
        if (recipes.Count != 0) {
            bool missingIngredients = false;
            for (int index = 0; index < recipes[0].ingredients.Count; ++index) {
                if (recipes[0].ingredients[index].itemValue.ItemClass.GetItemName() != this.GetBlockName()) {
                    if (playerUi.xui.PlayerInventory.GetItemCount(recipes[0].ingredients[index].itemValue) < recipes[0].ingredients[index].count)
                    {
                        playerUi.xui.CollectedItemList.AddItemStack(new ItemStack(recipes[0].ingredients[index].itemValue, 0), true);
                        GameManager.ShowTooltip(_player as EntityPlayerLocal, Localization.Get("ttMissingCraftingResources", ""));
                        missingIngredients = true;
                    }
                }
            }
            if (missingIngredients)
                return true;
            for (int index = 0; index < recipes[0].ingredients.Count; ++index) {
                if (recipes[0].ingredients[index].itemValue.ItemClass.GetItemName() != this.GetBlockName()) {
                    playerUi.xui.PlayerInventory.RemoveItem(recipes[0].ingredients[index]);
                }
            }
        }
        Block blockByName = Block.GetBlockByName(this.buildableList[_indexInBlockActivationCommands - 1]);
        if (blockByName.Properties.Values.ContainsKey("ConfigureDelay"))
            this.configureDelay = StringParsers.ParseFloat(blockByName.Properties.Values["ConfigureDelay"], 0, -1, NumberStyles.Any);
        playerUi.windowManager.Open("timer", true, false, true);
        XUiC_Timer childByType = playerUi.xui.GetChildByType<XUiC_Timer>();
        TimerEventData _eventData = new TimerEventData();
        _eventData.CloseEvent += this.EventData_CloseEvent;
        _eventData.Data = (object)new object[5]
        {
          (int) _cIdx,
          (Vector3i) _blockPos,
          (byte) _blockValue.rotation,
          (byte) _blockValue.meta,
          (int) blockByName.blockID
        };
        _eventData.Event += this.EventData_Event;
        childByType.SetTimer(this.configureDelay, _eventData);
        return true;
    }
    private void EventData_CloseEvent(TimerEventData timerData)
    {
        this.ResetEventData(timerData);
    }
    private void EventData_Event(TimerEventData timerData)
    {
        World _world = GameManager.Instance.World;
        int _cIdx = (int)((object[])timerData.Data)[0];
        Vector3i _blockPos = (Vector3i)((object[])timerData.Data)[1];
        byte _blockRotation = (byte)((object[])timerData.Data)[2];
        byte _blockMeta = (byte)((object[])timerData.Data)[3];
        int _blockID = (int)((object[])timerData.Data)[4];
        BlockValue blockValue = new BlockValue() { type = _blockID };
        blockValue.rotation = _blockRotation;
        blockValue.meta = _blockMeta;
        _world.SetBlockRPC(_cIdx, _blockPos, blockValue, Block.list[blockValue.type].Density);
        this.ResetEventData(timerData);
    }
    private void ResetEventData(TimerEventData timerData)
    {
        timerData.CloseEvent -= this.EventData_CloseEvent;
        timerData.Event -= this.EventData_Event;
    }
}