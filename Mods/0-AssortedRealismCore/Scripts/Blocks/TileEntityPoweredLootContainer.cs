using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class TileEntityPoweredLootContainer : TileEntityPoweredBlock
{
    private bool isToggled = true;
    public float DelayTime;
    protected float updateTime;
    public int lootListIndex;
    private Vector2i containerSize;
    private ItemStack[] itemsArr;
    public bool bTouched;
    public ulong worldTimeTouched;
    public bool bPlayerBackpack;
    public bool bPlayerStorage;
    public bool bWasTouched;
    private List<Entity> entList;
    public ItemStack[] items {
        get {
            if (this.itemsArr == null)
                this.itemsArr = ItemStack.CreateArray(this.containerSize.x * this.containerSize.y);
            return this.itemsArr;
        } set {
            this.itemsArr = value;
        }
    }
    public TileEntityPoweredLootContainer(Chunk _chunk) : base(_chunk) {
        this.containerSize = new Vector2i(3, 3);
        this.lootListIndex = 0;
    }
    private TileEntityPoweredLootContainer(TileEntityPoweredLootContainer _other) : base((Chunk) null) {
        this.lootListIndex = _other.lootListIndex;
        this.containerSize = _other.containerSize;
        this.items = ItemStack.Clone((IList<ItemStack>) _other.items);
        this.bTouched = _other.bTouched;
        this.worldTimeTouched = _other.worldTimeTouched;
        this.bPlayerBackpack = _other.bPlayerBackpack;
        this.bPlayerStorage = _other.bPlayerStorage;
        this.bUserAccessing = _other.bUserAccessing;
    }
    public TileEntityPoweredLootContainer Clone() {
        return new TileEntityPoweredLootContainer(this);
    }
    public void CopyLootContainerDataFromOther(TileEntityPoweredLootContainer _other) {
        this.lootListIndex = _other.lootListIndex;
        this.containerSize = _other.containerSize;
        this.items = ItemStack.Clone((IList<ItemStack>) _other.items);
        this.bTouched = _other.bTouched;
        this.worldTimeTouched = _other.worldTimeTouched;
        this.bPlayerBackpack = _other.bPlayerBackpack;
        this.bPlayerStorage = _other.bPlayerStorage;
        this.bUserAccessing = _other.bUserAccessing;
    }
    public bool IsToggled
    {
        get {
            if (SingletonMonoBehaviour<ConnectionManager>.Instance.IsServer && this.PowerItem is PowerConsumerToggle)
                return (this.PowerItem as PowerConsumerToggle).IsToggled;
            return this.isToggled;
        } set {
            if (SingletonMonoBehaviour<ConnectionManager>.Instance.IsServer) {
                if (this.PowerItem is PowerConsumerToggle)
                    (this.PowerItem as PowerConsumerToggle).IsToggled = value;
                this.isToggled = value;
                this.SetModified();
            } else {
                this.isToggled = value;
                this.SetModified();
            }
        }
    }
    public override int PowerUsed {
        get {
            if (this.IsToggled)
                return base.PowerUsed;
            return 0;
        }
    }
    public override void UpdateTick(World world) {
        base.UpdateTick(world);
        if (GamePrefs.GetInt(EnumGamePrefs.LootRespawnDays) <= 0 || this.bPlayerStorage || (!this.bTouched || !this.IsEmpty()))
            return;
        int num = GameUtils.WorldTimeToHours(this.worldTimeTouched) + GameUtils.WorldTimeToDays(this.worldTimeTouched) * 24;
        if ((GameUtils.WorldTimeToHours(world.worldTime) + GameUtils.WorldTimeToDays(world.worldTime) * 24 - num) / 24 < GamePrefs.GetInt(EnumGamePrefs.LootRespawnDays)) {
            if (this.entList == null)
                this.entList = new List<Entity>();
            else
                this.entList.Clear();
            world.GetEntitiesInBounds(typeof (EntityPlayer), new Bounds(this.ToWorldPos().ToVector3(), Vector3.one * 16f), this.entList);
            if (this.entList.Count <= 0)
                return;
            this.worldTimeTouched = world.worldTime;
            this.setModified();
        } else {
            this.bWasTouched = false;
            this.bTouched = false;
            this.setModified();
        }
    }
    public Vector2i GetContainerSize() {
        return this.containerSize;
    }
    public void SetContainerSize(Vector2i _containerSize, bool clearItems = true)
    {
        this.containerSize = _containerSize;
        if (!clearItems)
            return;
        if (this.containerSize.x * this.containerSize.y != this.items.Length) {
            this.items = ItemStack.CreateArray(this.containerSize.x * this.containerSize.y);
        } else {
            for (int index = 0; index < this.items.Length; ++index)
                this.items[index] = ItemStack.Empty.Clone();
        }
    }
    public float GetOpenTime()
    {
        return LootContainer.lootList[this.lootListIndex].openTime;
    }

    public override bool Activate(bool activated) {
        World world = GameManager.Instance.World;
        BlockValue block = this.chunk.GetBlock(this.localChunkPos);
        return Block.list[block.type].ActivateBlock((WorldBase) world, this.GetClrIdx(), this.ToWorldPos(), block, activated, activated);
    }
    public override bool ActivateOnce() {
        World world = GameManager.Instance.World;
        BlockValue block = this.chunk.GetBlock(this.localChunkPos);
        return Block.list[block.type].ActivateBlockOnce((WorldBase) world, this.GetClrIdx(), this.ToWorldPos(), block);
    }

    public override void OnRemove(World world) {
        base.OnRemove(world);
        this.OnDestroy();
        if (!PowerManager.Instance.ClientUpdateList.Contains(this))
            return;
        PowerManager.Instance.ClientUpdateList.Remove(this);
    }
    public override void OnUnload(World world) {
        base.OnUnload(world);
        if (!PowerManager.Instance.ClientUpdateList.Contains(this))
            return;
        PowerManager.Instance.ClientUpdateList.Remove(this);
    }
    public override void OnSetLocalChunkPosition() {
        base.OnSetLocalChunkPosition();
        if ((UnityEngine.Object) GameManager.Instance == (UnityEngine.Object) null)
            return;
        World world = GameManager.Instance.World;
        if (this.chunk == null)
            return;
        BlockValue block1 = this.chunk.GetBlock(this.localChunkPos);
        Block block2 = Block.list[block1.type];
        if (!SingletonMonoBehaviour<ConnectionManager>.Instance.IsServer)
            return;
        block2.ActivateBlock((WorldBase) world, this.GetClrIdx(), this.ToWorldPos(), block1, this.IsPowered, this.IsPowered);
    }
    public virtual void ClientUpdate() {
    }
    public override void read(BinaryReader _br, TileEntity.StreamModeRead _eStreamMode)
    {
        base.read(_br, _eStreamMode);
        switch (_eStreamMode) {
            case TileEntity.StreamModeRead.Persistency:
                break;
            case TileEntity.StreamModeRead.FromClient:
                this.isToggled = _br.ReadBoolean();
                if (!(this.PowerItem is PowerConsumerToggle))
                    break;
                (this.PowerItem as PowerConsumerToggle).IsToggled = this.isToggled;
                break;
            default:
                this.isToggled = _br.ReadBoolean();
                break;
        }
        this.lootListIndex = (int) _br.ReadUInt16();
        this.containerSize = new Vector2i();
        this.containerSize.x = (int) _br.ReadUInt16();
        this.containerSize.y = (int) _br.ReadUInt16();
        this.bTouched = _br.ReadBoolean();
        this.worldTimeTouched = (ulong) _br.ReadUInt32();
        this.bPlayerBackpack = _br.ReadBoolean();
        int num = Math.Min((int) _br.ReadInt16(), this.containerSize.x * this.containerSize.y);
        if (this.bUserAccessing) {
            ItemStack itemStack = ItemStack.Empty.Clone();
            if (_eStreamMode == TileEntity.StreamModeRead.Persistency && this.readVersion < 3) {
                for (int index = 0; index < num; ++index)
                    itemStack.ReadOld(_br);
            } else {
                for (int index = 0; index < num; ++index)
                    itemStack.Read(_br);
            }
        } else {
            if (this.containerSize.x * this.containerSize.y != this.items.Length)
                this.items = ItemStack.CreateArray(this.containerSize.x * this.containerSize.y);
            if (_eStreamMode == TileEntity.StreamModeRead.Persistency && this.readVersion < 3) {
                for (int index = 0; index < num; ++index) {
                    this.items[index].Clear();
                    this.items[index].ReadOld(_br);
                }
            } else {
                for (int index = 0; index < num; ++index) {
                    this.items[index].Clear();
                    this.items[index].Read(_br);
                }
            }
        }
        this.bPlayerStorage = _br.ReadBoolean();
    }
    public override void write(BinaryWriter _bw, TileEntity.StreamModeWrite _eStreamMode)
    {
        base.write(_bw, _eStreamMode);
        if (_eStreamMode == TileEntity.StreamModeWrite.Persistency)
            return;
        _bw.Write(this.IsToggled);
        _bw.Write((ushort) this.lootListIndex);
        _bw.Write((ushort) this.containerSize.x);
        _bw.Write((ushort) this.containerSize.y);
        _bw.Write(this.bTouched);
        _bw.Write((uint) this.worldTimeTouched);
        _bw.Write(this.bPlayerBackpack);
        _bw.Write((short) this.items.Length);
        foreach (ItemStack itemStack in this.items)
            itemStack.Clone().Write(_bw);
        _bw.Write(this.bPlayerStorage);
    }
    public override TileEntityType GetTileEntityType() {
        return TileEntityType.Loot;
    }
    public override void UpgradeDowngradeFrom(TileEntity _other) {
        base.UpgradeDowngradeFrom(_other);
        this.OnDestroy();
        if (!(_other is TileEntityPoweredLootContainer))
            return;
        TileEntityPoweredLootContainer entityLootContainer = _other as TileEntityPoweredLootContainer;
        this.bTouched = entityLootContainer.bTouched;
        this.worldTimeTouched = entityLootContainer.worldTimeTouched;
        this.bPlayerBackpack = entityLootContainer.bPlayerBackpack;
        this.bPlayerStorage = entityLootContainer.bPlayerStorage;
        this.items = ItemStack.Clone(entityLootContainer.items, 0, this.containerSize.x * this.containerSize.y);
        if (this.items.Length == this.containerSize.x * this.containerSize.y)
            return;
        Log.Error("UpgradeDowngradeFrom: other.size={0}, other.length={1}, this.size={2}, this.length={3}", (object) entityLootContainer.containerSize, (object) entityLootContainer.items.Length, (object) this.containerSize, (object) this.items.Length);
    }
    public void UpdateSlot(int _idx, ItemStack _item) {
        this.items[_idx] = _item.Clone();
        this.tileEntityChanged();
    }
    private void tileEntityChanged() {
        for (int index = 0; index < this.listeners.Count; ++index)
            this.listeners[index].OnTileEntityChanged((TileEntity) this, 0);
    }
    public bool IsEmpty() {
        for (int index = 0; index < this.items.Length; ++index) {
            if (!this.items[index].IsEmpty())
                return false;
        }
        return true;
    }
    public void SetEmpty() {
        for (int index = 0; index < this.items.Length; ++index)
            this.items[index].Clear();
        this.tileEntityChanged();
        this.bTouched = true;
        this.setModified();
    }
    public bool TryStackItem(int startIndex, ItemStack _itemStack) {
        for (int index = startIndex; index < this.items.Length; ++index) {
            int count = _itemStack.count;
            if (_itemStack.itemValue.type == this.items[index].itemValue.type && this.items[index].CanStackPartly(ref count)) {
                this.items[index].count += count;
                _itemStack.count -= count;
                this.setModified();
                if (_itemStack.count == 0) {
                    this.tileEntityChanged();
                    return true;
                }
            }
        }
        return false;
    }
    public bool AddItem(ItemStack _item) {
        for (int _idx = 0; _idx < this.items.Length; ++_idx) {
            if (this.items[_idx].IsEmpty()) {
                this.UpdateSlot(_idx, _item);
                this.tileEntityChanged();
                return true;
            }
        }
        return false;
    }
    public bool HasItem(ItemValue _item) {
        for (int index = 0; index < this.items.Length; ++index) {
            if (this.items[index].itemValue.ItemClass == _item.ItemClass)
                return true;
        }
        return false;
    }
    public void RemoveItem(ItemValue _item) {
        bool flag = false;
        for (int _idx = 0; _idx < this.items.Length; ++_idx) {
            if (this.items[_idx].itemValue.ItemClass == _item.ItemClass) {
                this.UpdateSlot(_idx, ItemStack.Empty.Clone());
                flag = true;
            }
        }
        if (!flag)
            return;
        this.tileEntityChanged();
    }
    public override void Reset() {
        base.Reset();
        if (this.bPlayerStorage || this.bPlayerBackpack)
            return;
        this.bTouched = false;
        this.bWasTouched = false;
        for (int index = 0; index < this.items.Length; ++index)
            this.items[index].Clear();
        this.setModified();
    }
    public ItemStack[] GetItems() {
        return this.items;
    }
}