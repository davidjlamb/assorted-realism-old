using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using GUI_2;

public class BlockPoweredLoot : BlockPowered
{
    protected static string PropLootList = "LootList";
    private BlockActivationCommand[] cmds = new BlockActivationCommand[2]{ new BlockActivationCommand("Search", "search", true), new BlockActivationCommand("take", "hand", false) };
    protected int lootList;
    public BlockPoweredLoot() {
        this.HasTileEntity = true;
    }
    public override void Init() {
        base.Init();
        if (!this.Properties.Values.ContainsKey(BlockPoweredLoot.PropLootList))
            throw new Exception("Block with name " + this.GetBlockName() + " doesnt have a loot list");
        int.TryParse(this.Properties.Values[BlockPoweredLoot.PropLootList], out this.lootList);
    }

    public override void PlaceBlock(WorldBase _world, BlockPlacement.Result _result, EntityAlive _ea) {
        base.PlaceBlock(_world, _result, _ea);
        TileEntityPoweredLootContainer tileEntity = _world.GetTileEntity(_result.clrIdx, _result.blockPos) as TileEntityPoweredLootContainer;
        if (tileEntity == null || !((UnityEngine.Object) _ea != (UnityEngine.Object) null) || _ea.entityType != EntityType.Player)
            return;
        tileEntity.bPlayerStorage = true;
        tileEntity.worldTimeTouched = _world.GetWorldTime();
        tileEntity.SetEmpty();
    }
    public override string GetActivationText(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _entityFocusing) {
        TileEntityPoweredLootContainer tileEntity = _world.GetTileEntity(_clrIdx, _blockPos) as TileEntityPoweredLootContainer;
        if (tileEntity == null)
            return "string.Empty";
        string str1 = Localization.Get(Block.list[_blockValue.type].GetBlockName(), "");
        PlayerActionsLocal playerInput = ((EntityPlayerLocal) _entityFocusing).playerInput;
        string str2 = playerInput.Activate.GetBindingXuiMarkupString(XUiUtils.EmptyBindingStyle.EmptyString, XUiUtils.DisplayStyle.Plain) + playerInput.PermanentActions.Activate.GetBindingXuiMarkupString(XUiUtils.EmptyBindingStyle.EmptyString, XUiUtils.DisplayStyle.Plain);
        if (!tileEntity.bTouched)
            return string.Format(Localization.Get("lootTooltipNew", ""), (object) str2, (object) str1);
        if (tileEntity.IsEmpty())
            return string.Format(Localization.Get("lootTooltipEmpty", ""), (object) str2, (object) str1);
        return string.Format(Localization.Get("lootTooltipTouched", ""), (object) str2, (object) str1);
    }
    public override void OnBlockAdded(WorldBase world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue) {
        base.OnBlockAdded(world, _chunk, _blockPos, _blockValue);
        if (_blockValue.ischild)
            return;
        if (this.lootList >= LootContainer.lootList.Length || LootContainer.lootList[this.lootList] == null)
            Log.Error("No loot entry defined for loot list id {0}", (object) this.lootList);
        else
            this.addTileEntity(world, _chunk, _blockPos, _blockValue);
    }
    public override void OnBlockRemoved(WorldBase world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue) {
        base.OnBlockRemoved(world, _chunk, _blockPos, _blockValue);
        (world.GetTileEntity(_chunk.ClrIdx, _blockPos) as TileEntityPoweredLootContainer).OnDestroy();
        this.removeTileEntity(world, _chunk, _blockPos, _blockValue);
    }
    protected virtual void addTileEntity(WorldBase world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue) {
        TileEntityPoweredLootContainer entityLootContainer = new TileEntityPoweredLootContainer(_chunk);
        entityLootContainer.localChunkPos = World.toBlock(_blockPos);
        entityLootContainer.lootListIndex = (int) (ushort) this.lootList;
        entityLootContainer.SetContainerSize(LootContainer.lootList[this.lootList].size, true);
        _chunk.AddTileEntity((TileEntity) entityLootContainer);
    }
    protected virtual void removeTileEntity(WorldBase world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue) {
        _chunk.RemoveTileEntityAt<TileEntityPoweredLootContainer>((World) world, World.toBlock(_blockPos));
    }
    public override Block.DestroyedResult OnBlockDestroyedBy(WorldBase _world, int _clrIdx, Vector3i _blockPos, BlockValue _blockValue, int _entityId, bool _bUseHarvestTool) {
        TileEntityPoweredLootContainer tileEntity = _world.GetTileEntity(_clrIdx, _blockPos) as TileEntityPoweredLootContainer;
        tileEntity.OnDestroy();
        LocalPlayerUI uiForPlayer = LocalPlayerUI.GetUIForPlayer(GameManager.Instance.World.GetEntity(_entityId) as EntityPlayerLocal);
        if ((UnityEngine.Object) null != (UnityEngine.Object) uiForPlayer && uiForPlayer.windowManager.IsWindowOpen("looting") && ((XUiC_LootWindow) uiForPlayer.xui.GetWindow("windowLooting").Controller).GetLootBlockPos() == _blockPos)
            uiForPlayer.windowManager.Close("looting");
        if (tileEntity != null)
            _world.GetGameManager().DropContentOfLootContainerServer(_blockValue, _blockPos, tileEntity.entityId);
        return Block.DestroyedResult.Downgrade;
    }
    public override bool OnBlockActivated(WorldBase _world, int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player) {
        if (_player.inventory.IsHoldingItemActionRunning())
            return false;
        TileEntityPoweredLootContainer tileEntity = _world.GetTileEntity(_cIdx, _blockPos) as TileEntityPoweredLootContainer;
        if (tileEntity == null) {
            Debug.Log("tileEntity null");
            return false;
        }
        _player.AimingGun = false;
        Vector3i worldPos = tileEntity.ToWorldPos();
        tileEntity.bWasTouched = tileEntity.bTouched;
        _world.GetGameManager().TELockServer(_cIdx, worldPos, tileEntity.entityId, _player.entityId);
        return true;
    }
    public override bool OnBlockActivated(int _indexInBlockActivationCommands, WorldBase _world, int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player) {
        if (_blockValue.ischild) {
            Vector3i parentPos = Block.list[_blockValue.type].multiBlockPos.GetParentPos(_blockPos, _blockValue);
            BlockValue block = _world.GetBlock(parentPos);
            return this.OnBlockActivated(_indexInBlockActivationCommands, _world, _cIdx, parentPos, block, _player);
        }
        Debug.Log("Index: " + _indexInBlockActivationCommands);
        if (_indexInBlockActivationCommands == 0)
            return this.OnBlockActivated(_world, _cIdx, _blockPos, _blockValue, _player);
        if (_indexInBlockActivationCommands != 1)
            return false;
        this.TakeItemWithTimer(_cIdx, _blockPos, _blockValue, _player);
        return true;
    }
    public void TakeItemWithTimer(int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player) {
        if (_blockValue.damage > 0) {
            GameManager.ShowTooltipWithAlert(_player as EntityPlayerLocal, Localization.Get("ttRepairBeforePickup", ""), "ui_denied");
        } else {
            LocalPlayerUI playerUi = (_player as EntityPlayerLocal).PlayerUI;
            playerUi.windowManager.Open("timer", true, false, true);
            XUiC_Timer childByType = playerUi.xui.GetChildByType<XUiC_Timer>();
            TimerEventData _eventData = new TimerEventData();
            _eventData.Data = (object) new object[4] {
                (object) _cIdx,
                (object) _blockValue,
                (object) _blockPos,
                (object) _player
            };
            _eventData.Event += new TimerEventHandler(this.EventData_Event);
            childByType.SetTimer(this.TakeDelay, _eventData, -1f, "");
        }
    }

    private void EventData_Event(TimerEventData timerData) {
        World world = GameManager.Instance.World;
        object[] data = (object[]) timerData.Data;
        int _clrIdx = (int) data[0];
        BlockValue blockValue = (BlockValue) data[1];
        Vector3i vector3i = (Vector3i) data[2];
        BlockValue block = world.GetBlock(vector3i);
        EntityPlayerLocal entityPlayerLocal = data[3] as EntityPlayerLocal;
        if (block.damage > 0)
            GameManager.ShowTooltipWithAlert(entityPlayerLocal, Localization.Get("ttRepairBeforePickup", ""), "ui_denied");
        else if (block.type != blockValue.type) {
            GameManager.ShowTooltipWithAlert(entityPlayerLocal, Localization.Get("ttBlockMissingPickup", ""), "ui_denied");
        } else {
            TileEntityPoweredLootContainer tileEntity = world.GetTileEntity(_clrIdx, vector3i) as TileEntityPoweredLootContainer;
            if (tileEntity != null && tileEntity.IsUserAccessing()) {
                GameManager.ShowTooltipWithAlert(entityPlayerLocal, Localization.Get("ttCantPickupInUse", ""), "ui_denied");
            } else {
                LocalPlayerUI uiForPlayer = LocalPlayerUI.GetUIForPlayer(entityPlayerLocal);
                this.HandleTakeInternalItems(tileEntity, uiForPlayer);
                ItemStack itemStack = new ItemStack(block.ToItemValue(), 1);
                if (!uiForPlayer.xui.PlayerInventory.AddItem(itemStack, true))
                    uiForPlayer.xui.PlayerInventory.DropItem(itemStack);
                world.SetBlockRPC(_clrIdx, vector3i, BlockValue.Air);
            }
        }
    }

    protected virtual void HandleTakeInternalItems(TileEntityPoweredLootContainer te, LocalPlayerUI playerUI) {
    }

    public override BlockActivationCommand[] GetBlockActivationCommands(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _entityFocusing) {
        bool flag = _world.IsMyLandProtectedBlock(_blockPos, _world.GetGameManager().GetPersistentLocalPlayer(), false);
        if (_blockValue.ischild) {
            Vector3i parentPos = Block.list[_blockValue.type].multiBlockPos.GetParentPos(_blockPos, _blockValue);
            BlockValue block = _world.GetBlock(parentPos);
            return this.GetBlockActivationCommands(_world, block, _clrIdx, parentPos, _entityFocusing);
        }
        this.cmds[1].enabled = flag;
        return this.cmds;
    }
    public override TileEntityPowered CreateTileEntity(Chunk chunk)
    {
        PowerItem.PowerItemTypes powerItemTypes = PowerItem.PowerItemTypes.ConsumerToggle;
        TileEntityPoweredLootContainer entityPoweredBlock = new TileEntityPoweredLootContainer(chunk);
        entityPoweredBlock.PowerItemType = powerItemTypes;
        return (TileEntityPowered) entityPoweredBlock;
    }
}