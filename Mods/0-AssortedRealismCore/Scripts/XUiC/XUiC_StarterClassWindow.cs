using System.Collections.Generic;
using UnityEngine;
public class XUiC_StarterClassWindow : XUiController {
    private string text = "";
    private string title = "";
    public override bool GetBindingValue(ref string value, BindingItem binding) {
        string fieldName = binding.FieldName;
        if (!(fieldName == "text"))
        {
            if (!(fieldName == "title"))
                return false;
            value = this.title;
            return true;
        }
        value = this.text;
        return true;
    }
    public override void Init() {
        base.Init();
        this.GetChildById("btnBlacksmith").ViewComponent.Controller.OnPress += new XUiEvent_OnPressEventHandler(this.btnBlacksmith_OnPress);
        this.GetChildById("btnBuilder").ViewComponent.Controller.OnPress += new XUiEvent_OnPressEventHandler(this.btnBuilder_OnPress);
        this.GetChildById("btnCook").ViewComponent.Controller.OnPress += new XUiEvent_OnPressEventHandler(this.btnCook_OnPress);
        this.GetChildById("btnEngineer").ViewComponent.Controller.OnPress += new XUiEvent_OnPressEventHandler(this.btnEngineer_OnPress);
        this.GetChildById("btnHunter").ViewComponent.Controller.OnPress += new XUiEvent_OnPressEventHandler(this.btnHunter_OnPress);
        this.GetChildById("btnMarksman").ViewComponent.Controller.OnPress += new XUiEvent_OnPressEventHandler(this.btnMarksman_OnPress);
        this.GetChildById("btnMechanic").ViewComponent.Controller.OnPress += new XUiEvent_OnPressEventHandler(this.btnMechanic_OnPress);
        this.GetChildById("btnMedic").ViewComponent.Controller.OnPress += new XUiEvent_OnPressEventHandler(this.btnMedic_OnPress);
        this.GetChildById("btnMerchant").ViewComponent.Controller.OnPress += new XUiEvent_OnPressEventHandler(this.btnMerchant_OnPress);
        this.GetChildById("btnChemist").ViewComponent.Controller.OnPress += new XUiEvent_OnPressEventHandler(this.btnChemist_OnPress);
        this.GetChildById("btnSoldier").ViewComponent.Controller.OnPress += new XUiEvent_OnPressEventHandler(this.btnSoldier_OnPress);
        this.GetChildById("btnSurvivalist").ViewComponent.Controller.OnPress += new XUiEvent_OnPressEventHandler(this.btnSurvivalist_OnPress);
    }
    private void btnBlacksmith_OnPress(XUiController _sender, OnPressEventArgs _e) {
        this.GivePlayerQuest("quest_StarterClassBlacksmith1");
        this.xui.playerUI.windowManager.Close(this.WindowGroup.ID);
    }
    private void btnBuilder_OnPress(XUiController _sender, OnPressEventArgs _e) {
        this.GivePlayerQuest("quest_StarterClassBuilder1");
        this.xui.playerUI.windowManager.Close(this.WindowGroup.ID);
    }
    private void btnCook_OnPress(XUiController _sender, OnPressEventArgs _e) {
        this.GivePlayerQuest("quest_StarterClassCook1");
        this.xui.playerUI.windowManager.Close(this.WindowGroup.ID);
    }
    private void btnEngineer_OnPress(XUiController _sender, OnPressEventArgs _e) {
        this.GivePlayerQuest("quest_StarterClassEngineer1");
        this.xui.playerUI.windowManager.Close(this.WindowGroup.ID);
    }
    private void btnHunter_OnPress(XUiController _sender, OnPressEventArgs _e) {
        this.GivePlayerQuest("quest_StarterClassHunter1");
        this.xui.playerUI.windowManager.Close(this.WindowGroup.ID);
    }
    private void btnMarksman_OnPress(XUiController _sender, OnPressEventArgs _e) {
        this.GivePlayerQuest("quest_StarterClassMarksman1");
        this.xui.playerUI.windowManager.Close(this.WindowGroup.ID);
    }
    private void btnMechanic_OnPress(XUiController _sender, OnPressEventArgs _e) {
        this.GivePlayerQuest("quest_StarterClassMechanic1");
        this.xui.playerUI.windowManager.Close(this.WindowGroup.ID);
    }
    private void btnMedic_OnPress(XUiController _sender, OnPressEventArgs _e) {
        this.GivePlayerQuest("quest_StarterClassMedic1");
        this.xui.playerUI.windowManager.Close(this.WindowGroup.ID);
    }
    private void btnMerchant_OnPress(XUiController _sender, OnPressEventArgs _e) {
        this.GivePlayerQuest("quest_StarterClassMerchant1");
        this.xui.playerUI.windowManager.Close(this.WindowGroup.ID);
    }
    private void btnChemist_OnPress(XUiController _sender, OnPressEventArgs _e) {
        this.GivePlayerQuest("quest_StarterClassChemist1");
        this.xui.playerUI.windowManager.Close(this.WindowGroup.ID);
    }
    private void btnSoldier_OnPress(XUiController _sender, OnPressEventArgs _e) {
        this.GivePlayerQuest("quest_StarterClassSoldier1");
        this.xui.playerUI.windowManager.Close(this.WindowGroup.ID);
    }
    private void btnSurvivalist_OnPress(XUiController _sender, OnPressEventArgs _e) {
        this.GivePlayerQuest("quest_StarterClassSurvivalist1");
        this.xui.playerUI.windowManager.Close(this.WindowGroup.ID);
    }
    public static void ShowWindow() {
        EntityPlayerLocal entityPlayer = XUiM_Player.GetPlayer() as EntityPlayerLocal;
        LocalPlayerUI uiForPlayer = LocalPlayerUI.GetUIForPlayer(entityPlayer);
        if (!((Object)uiForPlayer != (Object)null) || !((Object)uiForPlayer.xui != (Object)null))
            return;
        XUiC_StarterClassWindow childByType = uiForPlayer.xui.FindWindowGroupByName("starterClassWindow").GetChildByType<XUiC_StarterClassWindow>();
        //childByType.text = Localization.Get("arStarterClassWindow", "");
        //childByType.title = Localization.Get("arStarterClassWindow_title", "");
        uiForPlayer.windowManager.Open("starterClassWindow", true, true, true);
        if (!uiForPlayer.windowManager.IsWindowOpen("windowpaging"))
            return;
        uiForPlayer.windowManager.Close("windowpaging");
    }
    private void GivePlayerQuest(string playerClass) {
        if (!XUi.IsGameRunning())
            return;
        EntityPlayerLocal entityPlayer = XUiM_Player.GetPlayer() as EntityPlayerLocal;
        entityPlayer.QuestJournal.AddQuest(QuestClass.CreateQuest(playerClass));
    }
    public override void Update(float _dt) {
        base.Update(_dt);
        if (!this.IsDirty)
            return;
        this.RefreshBindings(true);
        this.IsDirty = false;
    }
}