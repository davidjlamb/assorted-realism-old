using System;
using System.Xml;
using System.Collections.Generic;
using System.Collections;
using System.Globalization;
using UnityEngine;
using Audio;
public class ItemActionAngling : ItemAction {
    private float anglingTime = 45f;
    private float anglingChance = 0.95f;
    private string anglingItem = "foodRawMeat";
    private int anglingAmount = 2;
    public override ItemActionData CreateModifierData(ItemInventoryData _invData, int _indexInEntityOfAction) {
        return (ItemActionData) new ItemActionAngling.MyInventoryData(_invData, _indexInEntityOfAction);
    }
    public override void ReadFrom(DynamicProperties _props) {
        base.ReadFrom(_props);
        if (this.Properties.Values.ContainsKey("AnglingTime"))
            this.anglingTime = StringParsers.ParseFloat(this.Properties.Values["AnglingTime"], 0, -1, NumberStyles.Any);
        if (this.Properties.Values.ContainsKey("AnglingChance"))
            this.anglingChance = StringParsers.ParseFloat(this.Properties.Values["AnglingChance"], 0, -1, NumberStyles.Any);
        if (this.Properties.Values.ContainsKey("AnglingItem"))
            this.anglingItem = this.Properties.Values["AnglingItem"];
        if (this.Properties.Values.ContainsKey("AnglingAmount"))
            this.anglingAmount = int.Parse(this.Properties.Values["AnglingAmount"]);
    }
    public override void StopHolding(ItemActionData _data) {
        ((ItemActionAngling.MyInventoryData) _data).bAnglingStarted = false;
    }
    public override void ExecuteAction(ItemActionData _actionData, bool _bReleased) {
        if (!_bReleased || (double) Time.time - (double) _actionData.lastUseTime < (double) this.Delay || this.IsActionRunning(_actionData))
            return;
        if (_actionData.invData.itemStack.itemValue.MaxUseTimes > 0 && (double) _actionData.invData.itemStack.itemValue.UseTimes >= (double) _actionData.invData.itemStack.itemValue.MaxUseTimes) {
            if (this.item.Properties.Values.ContainsKey(ItemClass.PropSoundJammed))
                Manager.PlayInsidePlayerHead(this.item.Properties.Values[ItemClass.PropSoundJammed], -1, 0.0f, false, false);
            GameManager.ShowTooltip(_actionData.invData.holdingEntity as EntityPlayerLocal, "ttItemNeedsRepair");
            return;
        }
        BlockValue water = Block.GetBlockValue("water", false);
        ItemActionAngling.MyInventoryData myInventoryData = (ItemActionAngling.MyInventoryData) _actionData;
        EntityAlive holdingEntity = myInventoryData.invData.holdingEntity;
        Ray lookRay1 = holdingEntity.GetLookRay();
        int modelLayer = holdingEntity.GetModelLayer();
        holdingEntity.SetModelLayer(2, false);
        Voxel.Raycast(myInventoryData.invData.world, lookRay1, 20f, 131, holdingEntity is EntityPlayer ? 0.2f : 0.4f);
        holdingEntity.SetModelLayer(modelLayer, false);
        WorldRayHitInfo voxelRayHitInfo = Voxel.voxelRayHitInfo;
        HitInfoDetails hit1 = voxelRayHitInfo.hit;
        BlockValue blockValue = voxelRayHitInfo.hit.blockValue;
        if (!blockValue.Equals(water)) {
            if (this.item.Properties.Values.ContainsKey(ItemClass.PropSoundJammed))
                Manager.PlayInsidePlayerHead(this.item.Properties.Values[ItemClass.PropSoundJammed], -1, 0.0f, false, false);
            GameManager.ShowTooltip(_actionData.invData.holdingEntity as EntityPlayerLocal, "ttAnglingNotByWater");
            return;
        }
        _actionData.lastUseTime = Time.time;
        if (this.UseAnimation) {
            _actionData.invData.holdingEntity.RightArmAnimationUse = true;
            if (this.soundStart != null)
                _actionData.invData.holdingEntity.PlayOneShot(this.soundStart, true);
            ((ItemActionAngling.MyInventoryData) _actionData).bAnglingStarted = true;
        } else
            this.ExecuteInstantAction(_actionData.invData.holdingEntity, _actionData.invData.itemStack, true, (XUiC_ItemStack) null);
    }
    public override bool ExecuteInstantAction(EntityAlive ent, ItemStack stack, bool isHeldItem, XUiC_ItemStack stackController) {
        if (stack.itemValue.MaxUseTimes > 0)
            stack.itemValue.UseTimes += EffectManager.GetValue(PassiveEffects.DegradationPerUse, stack.itemValue, 1f, ent, (Recipe) null, stack.itemValue.ItemClass.ItemTags, true, true, true, true, 1, true);
        LocalPlayerUI playerUi = LocalPlayerUI.GetUIForPlayer(ent as EntityPlayerLocal);
        playerUi.windowManager.Open("timer", true, false, true);
        XUiC_Timer childByType = playerUi.xui.GetChildByType<XUiC_Timer>();
        TimerEventData eventData = new TimerEventData();
        eventData.CloseEvent += this.EventData_CloseEvent;
        eventData.Data = (object)new object[1] {
            (object) ent
        };
        eventData.Event += this.EventData_Event;
        eventData.alternateTime = ent.rand.RandomRange(this.anglingTime * this.anglingChance, this.anglingTime + 1f);
        eventData.AlternateEvent += this.EventData_CloseEvent;
        childByType.SetTimer(this.anglingTime, eventData);
        ent.inventory.CallOnToolbeltChangedInternal();
        return true;
    }
    public override bool IsActionRunning(ItemActionData _actionData) {
        ItemActionAngling.MyInventoryData myInventoryData = (ItemActionAngling.MyInventoryData) _actionData;
        return myInventoryData.bAnglingStarted && (double) Time.time - (double) myInventoryData.lastUseTime < 2.0 * (double) AnimationDelayData.AnimationDelay[myInventoryData.invData.item.HoldType.Value].RayCast;
    }
    public override void OnHoldingUpdate(ItemActionData _actionData)
    {
        ItemActionAngling.MyInventoryData myInventoryData = (ItemActionAngling.MyInventoryData) _actionData;
        if (!myInventoryData.bAnglingStarted || (double) Time.time - (double) myInventoryData.lastUseTime < (double) AnimationDelayData.AnimationDelay[myInventoryData.invData.item.HoldType.Value].RayCast)
            return;
        myInventoryData.bAnglingStarted = false;
        if (_actionData.invData.itemStack.itemValue.MaxUseTimes > 0)
            _actionData.invData.itemStack.itemValue.UseTimes += EffectManager.GetValue(PassiveEffects.DegradationPerUse, _actionData.invData.itemStack.itemValue, 1f, _actionData.invData.holdingEntity, (Recipe) null, _actionData.invData.itemStack.itemValue.ItemClass.ItemTags, true, true, true, true, 1, true);
        LocalPlayerUI playerUi = LocalPlayerUI.GetUIForPlayer(_actionData.invData.holdingEntity as EntityPlayerLocal);
        playerUi.windowManager.Open("timer", true, false, true);
        XUiC_Timer childByType = playerUi.xui.GetChildByType<XUiC_Timer>();
        TimerEventData eventData = new TimerEventData();
        eventData.CloseEvent += this.EventData_CloseEvent;
        eventData.Data = (object)new object[1] {
            (object) _actionData.invData.holdingEntity
        };
        eventData.Event += this.EventData_Event;
        eventData.alternateTime = myInventoryData.invData.holdingEntity.rand.RandomRange(this.anglingTime * this.anglingChance, this.anglingTime + 1f);
        eventData.AlternateEvent += this.EventData_CloseEvent;
        childByType.SetTimer(this.anglingTime, eventData);
        myInventoryData.invData.holdingEntity.inventory.CallOnToolbeltChangedInternal();
    }
    private void EventData_CloseEvent(TimerEventData timerData) {
        object[] data = (object[])timerData.Data;
        EntityPlayerLocal entity = data[0] as EntityPlayerLocal;
        GameManager.ShowTooltip(entity, Localization.Get("ttAnglingFailed", ""));
        this.ResetEventData(timerData);
    }
    private void EventData_Event(TimerEventData timerData) {
        World world = GameManager.Instance.World;
        object[] data = (object[])timerData.Data;
        EntityPlayerLocal entity = data[0] as EntityPlayerLocal;
        LocalPlayerUI playerUi = LocalPlayerUI.GetUIForPlayer(entity);
        ItemValue itemValue = ItemClass.GetItem(anglingItem, false);
        ItemStack itemStack = new ItemStack(itemValue, anglingAmount);
        if (!playerUi.xui.PlayerInventory.AddItem(itemStack, true))
            playerUi.xui.PlayerInventory.DropItem(itemStack);
        this.ResetEventData(timerData);
    }
    private void ResetEventData(TimerEventData timerData) {
        timerData.AlternateEvent -= this.EventData_CloseEvent;
        timerData.CloseEvent -= this.EventData_CloseEvent;
        timerData.Event -= this.EventData_Event;
    }
    private bool isBesideWater(World _world, Vector3i _blockPos) {
        bool _besideWater = false;
        _besideWater |= Block.list[_world.GetBlock(_blockPos.x, _blockPos.y + 1, _blockPos.z).type].blockMaterial.IsLiquid;
        _besideWater |= Block.list[_world.GetBlock(_blockPos.x + 1, _blockPos.y, _blockPos.z).type].blockMaterial.IsLiquid;
        _besideWater |= Block.list[_world.GetBlock(_blockPos.x - 1, _blockPos.y, _blockPos.z).type].blockMaterial.IsLiquid;
        _besideWater |= Block.list[_world.GetBlock(_blockPos.x, _blockPos.y, _blockPos.z + 1).type].blockMaterial.IsLiquid;
        _besideWater |= Block.list[_world.GetBlock(_blockPos.x, _blockPos.y, _blockPos.z - 1).type].blockMaterial.IsLiquid;
        return _besideWater;
    }
    private class MyInventoryData : ItemActionData {
        public bool bAnglingStarted;
        public MyInventoryData(ItemInventoryData _invData, int _indexInEntityOfAction): base(_invData, _indexInEntityOfAction) {
        }
    }
}