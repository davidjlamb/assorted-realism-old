using System;
using System.Xml;
using System.Collections.Generic;

using UnityEngine;
//  Happens at the 7th hour
// 	<requirement name="RequirementEveryDawn, Mods" value="7"/>
public class ItemActionAROpenStarterClassBundle : ItemActionOpenBundle
{
    public string[] arAttribute;
    public string[] arAttributeCount;
    public override void ReadFrom(DynamicProperties _props) {
        base.ReadFrom(_props);
        if (_props.Values.ContainsKey("AR_attribute")) {
            this.arAttribute = _props.Values["AR_attribute"].Replace(" ", "").Split(',');
            if (_props.Values.ContainsKey("AR_attribute_count")) {
                this.arAttributeCount = _props.Values["AR_attribute_count"].Replace(" ", "").Split(',');
            } else {
                this.arAttributeCount = new string[0];
            }
        } else {
            this.arAttribute = (string[]) null;
            this.arAttributeCount = (string[]) null;
        }
    }
    public override bool ExecuteInstantAction(EntityAlive ent, ItemStack stack, bool isHeldItem, XUiC_ItemStack stackController) {
        base.ExecuteInstantAction(ent,stack,isHeldItem,stackController);
        EntityPlayerLocal entityPlayer = ent as EntityPlayerLocal;
        string _arClass = stack.itemValue.ItemClass.Name;
        Dictionary<string, int> items = new Dictionary<string, int>();
        if (_arClass == "arBuilderClassBundle") {
            items.Add("attPerception", 2);
            items.Add("perkdeadeye", 1);
            items.Add("perklockpicking", 1);
            items.Add("perkluckylooter", 1);
            items.Add("perkfiremansalmanacharvest", 1);
        }
        foreach(KeyValuePair<string, int> item in items) {
            ProgressionValue selectedSkill = entityPlayer.Progression.ProgressionValues[item.Key];
            if (selectedSkill.ProgressionClass.IsBook) {
                if (selectedSkill.Level != 1) {
                    ++selectedSkill.Level;
                    if (!SingletonMonoBehaviour<ConnectionManager>.Instance.IsServer) {
                        SingletonMonoBehaviour<ConnectionManager>.Instance.SendToServer((NetPackage) NetPackageManager.GetPackage<NetPackageEntitySetSkillLevelServer>().Setup(entityPlayer.entityId, selectedSkill.Name, selectedSkill.Level), false);
                    }
                    entityPlayer.bPlayerStatsChanged = true;
                }
            } else {
                if (selectedSkill.Level + item.Value > 10) {
                    selectedSkill.Level = 10;
                } else {
                    selectedSkill.Level += item.Value;
                }
                if (!SingletonMonoBehaviour<ConnectionManager>.Instance.IsServer) {
                    SingletonMonoBehaviour<ConnectionManager>.Instance.SendToServer((NetPackage) NetPackageManager.GetPackage<NetPackageEntitySetSkillLevelServer>().Setup(entityPlayer.entityId, selectedSkill.Name, selectedSkill.Level), false);
                }
                entityPlayer.bPlayerStatsChanged = true;
            }
        }
        return true;

        /*Debug.Log(stack.itemValue.ItemClass.Name);
        EntityPlayerLocal entityPlayer = ent as EntityPlayerLocal;
        foreach(KeyValuePair<string, ProgressionValue> bob in entityPlayer.Progression.ProgressionValues) {
            Debug.Log("PV: " + bob.Value.Name);
        }
        ProgressionValue selectedSkill = entityPlayer.Progression.ProgressionValues["attStrength"];
        foreach (ProgressionClass pc in selectedSkill.ProgressionClass.Children) {
            Debug.Log("PC: " + pc.NameKey);
            foreach (ProgressionClass pc2 in pc.Children) {
                Debug.Log("PC2: " + pc2.NameKey);
            }
        }*/
    }
}
