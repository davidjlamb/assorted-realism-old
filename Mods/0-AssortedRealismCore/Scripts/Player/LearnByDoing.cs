using System;
using System.Xml;
using System.Collections.Generic;

using Audio;
using UnityEngine;
public static class LearnByDoing {
    private static float levelMaxPoints = 500f;
    private static float levelMultiplier = 3f;
    public static ulong packMuleStart = 0;
    public static ulong packMuleDuration = 500;
    public static ulong wellInsulatedStart = 0;
    public static ulong wellInsulatedDuration = 500;
    public static ulong ruleOneCardioStart = 0;
    public static ulong ruleOneCardioDuration = 20;
    public static bool runAndGunAiming = false;
    public static bool parkourJumping = false;
    public static ulong fromTheShadowsStart = 0;
    public static ulong fromTheShadowsDuration = 100;
    private static float GetLevelMaxPoints(string perk = "other") {
        if (perk == "perkHealingFactor")
            return levelMaxPoints * 2f;
        if (perk == "perkSexualTrex")
            return levelMaxPoints * 4f;
        if (perk == "perkParkour")
            return levelMaxPoints * 2f;
        return levelMaxPoints;
    }


    /* DO NOT MODIFY BELOW THIS LINE!! */
    public static void GiveProgressionPoint(EntityPlayerLocal entity, string perk = "other") {
        ProgressionValue progressionValue = entity.Progression.GetProgressionValue(perk);
        if (progressionValue == null)
            return;
        float points = entity.GetCVar("_learnbydoing_" + perk);
        float maxPoints = GetLevelMaxPoints(perk);
        if (progressionValue.Level > 0)
            maxPoints = progressionValue.Level * maxPoints * levelMultiplier;
        if (points + 1f >= maxPoints) {
            points = points - (maxPoints + 1f);
            if (points < 0f)
                points = 0f;
            if (progressionValue.Level + 1 > progressionValue.ProgressionClass.MaxLevel)
                progressionValue.Level = progressionValue.ProgressionClass.MaxLevel;
            else
                progressionValue.Level += 1;
            if (progressionValue.ProgressionClass.IsPerk) {
                entity.MinEventContext.ProgressionValue = progressionValue;
                progressionValue.ProgressionClass.FireEvent(MinEventTypes.onPerkLevelChanged, entity.MinEventContext);
            }
            if (!SingletonMonoBehaviour<ConnectionManager>.Instance.IsServer)
                SingletonMonoBehaviour<ConnectionManager>.Instance.SendToServer((NetPackage) NetPackageManager.GetPackage<NetPackageEntitySetSkillLevelServer>().Setup(entity.entityId, progressionValue.Name, progressionValue.Level), false);
            Manager.PlayInsidePlayerHead("ui_skill_purchase", -1, 0.0f, false, false);
            entity.PlayerUI.xui.CollectedItemList.AddIconNotification(progressionValue.ProgressionClass.Icon, (int) 1, false);
            string ttPerk = Localization.Get(perk + "Name", "");
            string ttLevel = Localization.Get(perk + "Rank" + progressionValue.Level + "Desc", "");
            string ttText = string.Format(Localization.Get("ttYouHaveReached", ""), (object) ttPerk, (object) ttLevel);
            GameManager.ShowTooltip(entity, ttText);
                
        } else {
            points += 1f;
        }
        entity.SetCVar("_learnbydoing_" + perk, points);
        Debug.Log("LearnByDoing[" + perk + "]: " + points);
    }


    /*

    private static float classMaxPoints = 25f;
    private static float classLevelMultiplier = 3f;

    public static bool isRunning = false;
    public static bool isJumping = false;
    public static bool isCrouching = false;

    public static ulong encumberedStart = 0;
    public static ulong wellInsulatedStart = 0;

    public static void GivePlayerClassPoints(EntityPlayerLocal _player, string _class = "Other", float _points = 1f) {
        float currentPoints = _player.GetCVar("_lbd" + _class);
        int currentLevel = GetPlayerClassLevel(_player, _class);
        float pointsToNextLevel = GetClassMaxPoints(_class);
        if (currentLevel > 0)
            pointsToNextLevel = currentLevel * pointsToNextLevel * classLevelMultiplier;
        if (currentPoints + _points >= pointsToNextLevel) {
            _points = pointsToNextLevel - currentPoints;
            currentPoints = 0f;
            GivePlayerClassLevel(_player, _class);
        }
        currentPoints += _points;
        _player.SetCVar("_lbd" + _class, currentPoints);
        Debug.Log("LearnByDoing[" + _class + "]: " + currentPoints);
    }
    public static float GetClassMaxPoints(string _class = "Other") {
        if (_class == "Running")
            return classMaxPoints * 2f;
        if (_class == "Jumping")
            return classMaxPoints * 2f;
        return classMaxPoints;
    }
    public static int GetPlayerClassLevel(EntityPlayer _player, string _class = "Other") {
        ProgressionValue progressionValue = null;
        
        if (_class == "Harvesting")
            progressionValue = _player.Progression.GetProgressionValue("perkminer69r");
        
        if (_class == "Harvesting")
            progressionValue = _player.Progression.GetProgressionValue("perkminer69r");
        
        if (progressionValue != null)
            return progressionValue.Level;
        return 0;
    }
    public static void GivePlayerClassLevel(EntityPlayerLocal _player, string _class = "Other") {
        if ((bool) ((UnityEngine.Object) _player))
            _player.PlayerUI.xui.CollectedItemList.AddIconNotification("ui_game_symbol_lbd_" + _class, (int) 1, false);
        if (_class == "Harvesting") {
            GivePlayerProgressionLevel(_player, "perkminer69r");
            GivePlayerProgressionLevel(_player, "perkmotherlode");
        }
    }
    private static void GivePlayerProgressionLevel(EntityPlayer _player, string _progressionName)
    {
        ProgressionValue progressionValue = _player.Progression.GetProgressionValue(_progressionName);
        if (progressionValue == null)
            return;
        if (progressionValue.Level + 1 > progressionValue.ProgressionClass.MaxLevel)
            progressionValue.Level = progressionValue.ProgressionClass.MaxLevel;
        else
            progressionValue.Level += 1;
        if (progressionValue.ProgressionClass.IsPerk) {
            _player.MinEventContext.ProgressionValue = progressionValue;
            progressionValue.ProgressionClass.FireEvent(MinEventTypes.onPerkLevelChanged, _player.MinEventContext);
        }
        if (!SingletonMonoBehaviour<ConnectionManager>.Instance.IsServer)
            SingletonMonoBehaviour<ConnectionManager>.Instance.SendToServer((NetPackage) NetPackageManager.GetPackage<NetPackageEntitySetSkillLevelServer>().Setup(_player.entityId, progressionValue.Name, progressionValue.Level), false);
        Manager.PlayInsidePlayerHead("ui_skill_purchase", -1, 0.0f, false, false);
    }*/
}