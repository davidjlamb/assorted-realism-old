using System;
using SDX.Compiler;
using Mono.Cecil;
using Mono.Cecil.Cil;
using System.Linq;

public class AssortedRealism_TileEntityWorkstation : IPatcherMod {
   public bool Patch(ModuleDefinition module) {
       var gm = module.Types.First(d => d.Name == "TileEntityWorkstation");
       var method = gm.Methods.First(d => d.Name == "hasRecipeInQueue");
       SetMethodToPublic(method);
       var method2 = gm.Methods.First(d => d.Name == "cycleRecipeQueue");
       SetMethodToPublic(method2);
       var gm2 = module.Types.First(d => d.Name == "XUiC_WorkstationWindowGroup");
       var method3 = gm2.Fields.First(d => d.Name == "craftingQueue");
       SetFieldToPublic(method3);
       return true;
   }
   public bool Link(ModuleDefinition gameModule, ModuleDefinition modModule) {
       return true;
   }
   private void SetMethodToVirtual(MethodDefinition meth) {
       meth.IsVirtual = true;
   }
   private void SetFieldToPublic(FieldDefinition field) {
       field.IsFamily = false;
       field.IsPrivate = false;
       field.IsPublic = true;
   }
   private void SetMethodToPublic(MethodDefinition field) {
       field.IsFamily = false;
       field.IsPrivate = false;
       field.IsPublic = true;
   }
}