using System;
using SDX.Compiler;
using Mono.Cecil;
using Mono.Cecil.Cil;
using System.Linq;
using Mono.Cecil.Metadata;
using Mono.Collections.Generic;

public class AssortedRealism_ItemClass : IPatcherMod
{
   public bool Patch(ModuleDefinition module) {
        var gm = module.Types.First(d => d.Name == "ItemClass");
        FieldDefinition myField = new FieldDefinition("DegradationDropAfterBreak", FieldAttributes.Public, module.Import(typeof(string)));
        myField.InitialValue = System.Text.Encoding.Unicode.GetBytes("");
        gm.Fields.Add(myField);
        return true;
   }
   public bool Link(ModuleDefinition gameModule, ModuleDefinition modModule) {
       return true;
   }
   private void SetMethodToVirtual(MethodDefinition meth) {
       meth.IsVirtual = true;
   }
   private void SetFieldToPublic(FieldDefinition field) {
       field.IsFamily = false;
       field.IsPrivate = false;
       field.IsPublic = true;
   }
   private void SetMethodToPublic(MethodDefinition field) {
       field.IsFamily = false;
       field.IsPrivate = false;
       field.IsPublic = true;
   }
}