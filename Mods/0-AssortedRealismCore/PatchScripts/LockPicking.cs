using System;
using SDX.Compiler;
using Mono.Cecil;
using Mono.Cecil.Cil;
using System.Linq;
using Mono.Cecil.Metadata;
using Mono.Collections.Generic;

public class AssortedRealism_LockPicking : IPatcherMod
{
   public bool Patch(ModuleDefinition module) {
        var gm = module.Types.First(d => d.Name == "BlockSecureLoot");
        var method = gm.Methods.First(d => d.Name == "ResetEventData");
        SetMethodToPublic(method);
        return true;
   }
   public bool Link(ModuleDefinition gameModule, ModuleDefinition modModule) {
       return true;
   }
   private void SetMethodToVirtual(MethodDefinition meth) {
       meth.IsVirtual = true;
   }
   private void SetFieldToPublic(FieldDefinition field) {
       field.IsFamily = false;
       field.IsPrivate = false;
       field.IsPublic = true;
   }
   private void SetMethodToPublic(MethodDefinition field) {
       field.IsFamily = false;
       field.IsPrivate = false;
       field.IsPublic = true;
   }
}