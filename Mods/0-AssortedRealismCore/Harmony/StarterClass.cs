using Harmony;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DMT;

[HarmonyPatch(typeof(PlayerMoveController))]
[HarmonyPatch("InitialMessage_CloseEvent")]
public class AssortedRealism_PlayerMoveController_InitialMessage_CloseEvent {
    public static bool Prefix(Object obj, PlayerMoveController __instance) {
        XUiC_StarterClassWindow.ShowWindow();
        return false;
    }
}