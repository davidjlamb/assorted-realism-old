using Harmony;
using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DMT;

[HarmonyPatch(typeof(GUIWindowManager))]
[HarmonyPatch("Close")]
[HarmonyPatch(new Type[] { typeof(GUIWindow), typeof(bool) })]
public class AssortedRealism_GUIWindowManager_Close {
    public static bool Prefix(GUIWindow _w, GUIWindowManager __instance) {
        if (!(_w is XUiWindowGroup))
            return true;
        if (!((_w as XUiWindowGroup).Controller is XUiC_WorkstationWindowGroup))
            return true;
        XUiC_WorkstationWindowGroup workstation = ((XUiC_WorkstationWindowGroup) ((XUiWindowGroup) _w).Controller);
        if (workstation.craftingQueue.IsCrafting()) {
            GameManager.ShowTooltipWithAlert(workstation.xui.playerUI.entityPlayer, Localization.Get("ttCompleteQueueBeforeLeave", ""), "ui_denied");
            return false;
        }
        return true;
    }
}