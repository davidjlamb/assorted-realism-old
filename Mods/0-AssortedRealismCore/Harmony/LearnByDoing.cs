using Harmony;
using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DMT;

[HarmonyPatch(typeof(ItemActionAttack))]
[HarmonyPatch("Hit")]
public class arLearnByDoing_ItemActionAttack_Hit {
    private static FastTags weaponRangedGunTag = FastTags.Parse("weapon,ranged,gun");
    
    public static void Postfix(World _world, WorldRayHitInfo hitInfo, int _attackerEntityId, ItemValue damagingItemValue) {
        if (hitInfo == null || hitInfo.tag == null)
            return;
        if (!hitInfo.tag.StartsWith("E_"))
            return;
        Entity entity = _world.GetEntity(_attackerEntityId);
        if (!(entity is EntityPlayerLocal))
            return;
        if (!damagingItemValue.ItemClass.ItemTags.Test_AllSet(weaponRangedGunTag))
            return;
        if (!LearnByDoing.runAndGunAiming)
            LearnByDoing.GiveProgressionPoint(entity as EntityPlayerLocal, "perkRunAndGun");
        string bodyPartName;
        Entity entityNoTagCheck = ItemActionAttack.FindHitEntityNoTagCheck(hitInfo, out bodyPartName);
        if (bodyPartName == "head")
            LearnByDoing.GiveProgressionPoint(entity as EntityPlayerLocal, "perkDeadEye");
    }
}

[HarmonyPatch(typeof(EntityPlayerLocal))]
[HarmonyPatch("FireEvent")]
public class arLearnByDoing_EntityPlayerLocal_FireEvent {
    private static FastTags demolitionsExpertTag = FastTags.Parse("perkDemolitionsExpert");
    private static FastTags javelinMasterTag = FastTags.Parse("perkJavelinMaster");
    private static FastTags boomstickTag = FastTags.Parse("perkBoomstick");
    private static FastTags pummelPeteTag = FastTags.Parse("perkPummelPete");
    private static FastTags skullCrusherTag = FastTags.Parse("perkSkullCrusher");
    private static FastTags brawlerTag = FastTags.Parse("perkBrawler");
    private static FastTags machineGunnerTag = FastTags.Parse("perkMachineGunner");
    private static FastTags archeryTag = FastTags.Parse("perkArchery");
    private static FastTags gunslingerTag = FastTags.Parse("perkGunslinger");
    private static FastTags deepCutsTag = FastTags.Parse("perkDeepCuts");
    private static FastTags electrocutionerTag = FastTags.Parse("perkElectrocutioner");
    private static FastTags turretsTag = FastTags.Parse("perkTurrets");
    private static FastTags salvageOperationsTag = FastTags.Parse("perkSalvageOperations");
    private static FastTags sexualTyrannosaurusTag = FastTags.Parse("melee,tool");
    private static FastTags heavyArmorTag = FastTags.Parse("armor,heavyArmor");
    private static FastTags lightArmorTag = FastTags.Parse("armor,lightArmor");
    private static FastTags theHuntsmanTag = FastTags.Parse("perkTheHuntsman");
    private static FastTags livingOffTheLandTag = FastTags.Parse("food");
    private static FastTags healingFactorTag = FastTags.Parse("food");
    private static FastTags ironGutTag = FastTags.Parse("food");
    private static FastTags runAndGunTag = FastTags.Parse("weapon,ranged,gun");
    private static FastTags flurryOfBlowsTag = FastTags.Parse("perkFlurryOfBlows");
    private static FastTags physicianTag = FastTags.Parse("medical");
    public static void Postfix(MinEventTypes _eventType, bool useInventory, EntityPlayerLocal __instance) {
        if (_eventType != MinEventTypes.onSelfPrimaryActionUpdate && _eventType != MinEventTypes.onSelfSecondaryActionUpdate)
            Debug.Log(_eventType);
        
        // Combat Perks (except for dead eye)
        if (_eventType == MinEventTypes.onSelfKilledOther) {
            if (__instance.MinEventContext.ItemValue.ItemClass.ItemTags.Test_AllSet(demolitionsExpertTag))
                LearnByDoing.GiveProgressionPoint(__instance, "perkDemolitionsExpert");
            if (__instance.MinEventContext.ItemValue.ItemClass.ItemTags.Test_AllSet(javelinMasterTag))
                LearnByDoing.GiveProgressionPoint(__instance, "perkJavelinMaster");
            if (__instance.MinEventContext.ItemValue.ItemClass.ItemTags.Test_AllSet(boomstickTag))
                LearnByDoing.GiveProgressionPoint(__instance, "perkBoomstick");
            if (__instance.MinEventContext.ItemValue.ItemClass.ItemTags.Test_AllSet(pummelPeteTag))
                LearnByDoing.GiveProgressionPoint(__instance, "perkPummelPete");
            if (__instance.MinEventContext.ItemValue.ItemClass.ItemTags.Test_AllSet(skullCrusherTag))
                LearnByDoing.GiveProgressionPoint(__instance, "perkSkullCrusher");
            if (__instance.MinEventContext.ItemValue.ItemClass.ItemTags.Test_AllSet(brawlerTag))
                LearnByDoing.GiveProgressionPoint(__instance, "perkBrawler");
            if (__instance.MinEventContext.ItemValue.ItemClass.ItemTags.Test_AllSet(machineGunnerTag))
                LearnByDoing.GiveProgressionPoint(__instance, "perkMachineGunner");
            if (__instance.MinEventContext.ItemValue.ItemClass.ItemTags.Test_AllSet(archeryTag))
                LearnByDoing.GiveProgressionPoint(__instance, "perkArchery");
            if (__instance.MinEventContext.ItemValue.ItemClass.ItemTags.Test_AllSet(gunslingerTag))
                LearnByDoing.GiveProgressionPoint(__instance, "perkGunslinger");
            if (__instance.MinEventContext.ItemValue.ItemClass.ItemTags.Test_AllSet(deepCutsTag))
                LearnByDoing.GiveProgressionPoint(__instance, "perkDeepCuts");
            if (__instance.MinEventContext.ItemValue.ItemClass.ItemTags.Test_AllSet(electrocutionerTag))
                LearnByDoing.GiveProgressionPoint(__instance, "perkElectrocutioner");
            if (__instance.MinEventContext.ItemValue.ItemClass.ItemTags.Test_AllSet(turretsTag))
                LearnByDoing.GiveProgressionPoint(__instance, "perkTurrets");
        }

        // Animal Tracker
        if (_eventType == MinEventTypes.onSelfKilledOther) {
            if ((__instance.MinEventContext.Other is EntityAnimal)) {}
                LearnByDoing.GiveProgressionPoint(__instance, "perkAnimalTracker");
        }

        // The Penetrator
        if (_eventType == MinEventTypes.onSelfKilledOther) {
            // Not sure what to do here
        }

        // Lucky Looter
        if (_eventType == MinEventTypes.onSelfItemLooted)
            LearnByDoing.GiveProgressionPoint(__instance, "perkLuckyLooter");
        
        // Salvage Operations
        if (_eventType == MinEventTypes.onSelfHarvestBlock || _eventType == MinEventTypes.onSelfHarvestOther) {
            if (__instance.inventory.holdingItemItemValue.ItemClass.ItemTags.Test_AllSet(salvageOperationsTag))
                LearnByDoing.GiveProgressionPoint(__instance, "perkSalvageOperations");
        }

        // Sexual Tyrannosaurus
        // Not sure what to do here. Reduces melee/tool stamina usage. Maybe on primary action if melee/tool?
        if (_eventType == MinEventTypes.onSelfPrimaryActionEnd) {
            if (__instance.inventory.holdingItemItemValue.ItemClass.ItemTags.Test_AllSet(sexualTyrannosaurusTag))
                LearnByDoing.GiveProgressionPoint(__instance, "perkSexualTrex");
        }

        // Pack Mule Start
        if (_eventType == MinEventTypes.onSelfBuffStart)
            if (__instance.MinEventContext.Buff.BuffName == "buffEncumberedInv")
                if (LearnByDoing.packMuleStart == 0)
                    LearnByDoing.packMuleStart = GameManager.Instance.World.worldTime;

        // Pack Mule Stop
        if (_eventType == MinEventTypes.onSelfBuffFinish)
            if (__instance.MinEventContext.Buff.BuffName == "buffEncumberedInv")
                if (LearnByDoing.packMuleStart != 0) {
                    if (GameManager.Instance.World.worldTime - LearnByDoing.packMuleStart > LearnByDoing.packMuleDuration)
                        LearnByDoing.GiveProgressionPoint(__instance, "perkPackMule");
                    LearnByDoing.packMuleStart = 0;
                }

        // Miner 69'er & Mother Lode
        if (_eventType == MinEventTypes.onSelfDamagedBlock) {
            if (Array.IndexOf(__instance.inventory.holdingItemItemValue.ItemClass.Groups, "Tools/Traps") == -1)
                return;
            if (__instance.MinEventContext.BlockValue.Block.shape.IsTerrain()) {
                LearnByDoing.GiveProgressionPoint(__instance, "perkMiner69r");
                LearnByDoing.GiveProgressionPoint(__instance, "perkMotherLode");
            } else {
                LearnByDoing.GiveProgressionPoint(__instance, "perkMiner69r"); // this should be something else
                LearnByDoing.GiveProgressionPoint(__instance, "perkMotherLode");
            }
        }

        // Heavy Armor
        if (_eventType == MinEventTypes.onOtherDamagedSelf) {
            if (__instance.equipment != null) {
                List<ItemValue> armor = __instance.equipment.GetArmor();
                bool heavyArmor = false;
                if (armor.Count > 0) {
                    for (int index = 0; index < armor.Count; ++index) {
                        if (armor[index].ItemClass.ItemTags.Test_AllSet(heavyArmorTag))
                            heavyArmor = true;
                    }
                }
                if (heavyArmor)
                    LearnByDoing.GiveProgressionPoint(__instance, "perkHeavyArmor");
            }
        }

        // The Huntsman
        if (_eventType == MinEventTypes.onSelfHarvestOther) {
            if (__instance.inventory.holdingItemItemValue.ItemClass.ItemTags.Test_AllSet(theHuntsmanTag))
                LearnByDoing.GiveProgressionPoint(__instance, "perkTheHuntsman");
        }

        // Well Insulated
        if (_eventType == MinEventTypes.onSelfBuffStart)
            if (__instance.MinEventContext.Buff.BuffName == "buffElementHot" || __instance.MinEventContext.Buff.BuffName == "buffElementSweltering" || __instance.MinEventContext.Buff.BuffName == "buffElementCold" || __instance.MinEventContext.Buff.BuffName == "buffElementFreezing")
                if (LearnByDoing.wellInsulatedStart == 0)
                    LearnByDoing.wellInsulatedStart = GameManager.Instance.World.worldTime;
        if (_eventType == MinEventTypes.onSelfBuffFinish)
            if (__instance.MinEventContext.Buff.BuffName == "buffElementHot" || __instance.MinEventContext.Buff.BuffName == "buffElementSweltering" || __instance.MinEventContext.Buff.BuffName == "buffElementCold" || __instance.MinEventContext.Buff.BuffName == "buffElementFreezing")
                if (LearnByDoing.wellInsulatedStart != 0) {
                    if (GameManager.Instance.World.worldTime - LearnByDoing.wellInsulatedStart > LearnByDoing.wellInsulatedDuration)
                        LearnByDoing.GiveProgressionPoint(__instance, "perkWellInsulated");
                    LearnByDoing.wellInsulatedStart = 0;
                }
        
        // Living Off The Land
        if (_eventType == MinEventTypes.onSelfDestroyedBlock)
            if (__instance.MinEventContext.BlockValue.Block.IsPlant())
                if (Array.IndexOf(__instance.MinEventContext.BlockValue.Block.FilterTags, "fcrops") != -1)
                    LearnByDoing.GiveProgressionPoint(__instance, "perkLivingOffTheLand");

        // Pain Tolerance
        if (_eventType == MinEventTypes.onOtherDamagedSelf)
            LearnByDoing.GiveProgressionPoint(__instance, "perkPainTolerance");
        
        // Iron Gut
        if (_eventType == MinEventTypes.onSelfPrimaryActionEnd)
            if (__instance.MinEventContext.ItemValue.ItemClass.ItemTags.Test_AllSet(ironGutTag))
                LearnByDoing.GiveProgressionPoint(__instance, "perkSlowMetabolism");
        
        // Rule 1: Cardio
        if (_eventType == MinEventTypes.onSelfRun || _eventType == MinEventTypes.onSelfCrouchRun)
            if (LearnByDoing.ruleOneCardioStart == 0)
                LearnByDoing.ruleOneCardioStart = GameManager.Instance.World.worldTime;
        if (_eventType == MinEventTypes.onSelfWalk || _eventType == MinEventTypes.onSelfCrouchWalk)
            if (LearnByDoing.ruleOneCardioStart != 0) {
                if (GameManager.Instance.World.worldTime - LearnByDoing.ruleOneCardioStart > LearnByDoing.ruleOneCardioDuration)
                    LearnByDoing.GiveProgressionPoint(__instance, "perkRuleOneCardio");
                LearnByDoing.ruleOneCardioStart = 0;
            }

        // Run And Gun
        if (_eventType == MinEventTypes.onSelfAimingGunStart)
            LearnByDoing.runAndGunAiming = true;
        if (_eventType == MinEventTypes.onSelfAimingGunStop)
            LearnByDoing.runAndGunAiming = false;
        
        // Flurry Of Blows
        if (_eventType == MinEventTypes.onSelfKilledOther)
            if (__instance.inventory.holdingItemItemValue.ItemClass.ItemTags.Test_AllSet(flurryOfBlowsTag))
                LearnByDoing.GiveProgressionPoint(__instance, "perkFlurryOfBlows");

        // Light Armor
        if (_eventType == MinEventTypes.onOtherDamagedSelf) {
            if (__instance.equipment != null) {
                List<ItemValue> armor = __instance.equipment.GetArmor();
                bool lightArmor = false;
                if (armor.Count > 0) {
                    for (int index = 0; index < armor.Count; ++index) {
                        if (armor[index].ItemClass.ItemTags.Test_AllSet(lightArmorTag))
                            lightArmor = true;
                    }
                }
                if (lightArmor)
                    LearnByDoing.GiveProgressionPoint(__instance, "perkLightArmor");
            }
        }

        // Parkour
        // Should clean this up a little so it only triggers on long jumps.
        if (_eventType == MinEventTypes.onSelfLandJump)
            LearnByDoing.GiveProgressionPoint(__instance, "perkParkour");

        // Hidden Strike
        if (_eventType == MinEventTypes.onSelfDamagedOther)
            if (!__instance.MinEventContext.Other.IsAlert)
                LearnByDoing.GiveProgressionPoint(__instance, "perkHiddenStrike");
        
        // From The Shadows
        if (_eventType == MinEventTypes.onSelfCrouch && GameManager.Instance.World.IsDark())
            if (LearnByDoing.fromTheShadowsStart == 0)
                LearnByDoing.fromTheShadowsStart = GameManager.Instance.World.worldTime;
        if (_eventType == MinEventTypes.onSelfStand)
            if (LearnByDoing.fromTheShadowsStart != 0) {
                if (GameManager.Instance.World.worldTime - LearnByDoing.fromTheShadowsStart > LearnByDoing.fromTheShadowsDuration)
                    LearnByDoing.GiveProgressionPoint(__instance, "perkFromTheShadows");
                LearnByDoing.fromTheShadowsStart = 0;
            }

        // Better Barter
        if (_eventType == MinEventTypes.onSelfItemSold || _eventType == MinEventTypes.onSelfItemBought)
            LearnByDoing.GiveProgressionPoint(__instance, "perkBetterBarter");

        // Charismatic Nature
        // Need to work on this!? Can't auto get; needs to be assigned via quest
        if (_eventType == MinEventTypes.onSelfBuffStart)
            if (__instance.MinEventContext.Buff.BuffName == "CharismaticNature")
                LearnByDoing.GiveProgressionPoint(__instance, "perkCharismaticNature");

        //Physician
        if (_eventType == MinEventTypes.onSelfPrimaryActionEnd)
            if (__instance.MinEventContext.ItemValue.ItemClass.ItemTags.Test_AllSet(physicianTag))
                LearnByDoing.GiveProgressionPoint(__instance, "perkPhysician");
        
        /*
        // Place Block
        if (_eventType == MinEventTypes.onSelfPlaceBlock)
            LearnByDoing.GiveProgressionPoint(__instance, "PlaceBlock");
        */
    }
}

[HarmonyPatch(typeof(BlockSecureLoot))]
[HarmonyPatch("EventData_Event")]
public class arLearnByDoing_BlockSecureLoot_EventData_Event {
    public static void Postfix(TimerEventData timerData) {
        object[] data = (object[]) timerData.Data;
        EntityPlayerLocal entityPlayerLocal = data[3] as EntityPlayerLocal;
        LearnByDoing.GiveProgressionPoint(entityPlayerLocal, "perkLockPicking");
    }
}

[HarmonyPatch(typeof(BlockMine))]
[HarmonyPatch("OnEntityWalking")]
public class arLearnByDoing_BlockMine_OnEntityWalking {
    public static void Postfix(Entity entity) {
        if (!(entity is EntityPlayerLocal))
            return;
        LearnByDoing.GiveProgressionPoint(entity as EntityPlayerLocal, "perkInfiltrator");
    }
}

[HarmonyPatch(typeof(BlockTrapDoor))]
[HarmonyPatch("OnEntityCollidedWithBlock")]
public class arLearnByDoing_BlockTrapDoor_OnEntityCollidedWithBlock {
    public static void Postfix(Entity _targetEntity) {
        if (!(_targetEntity is EntityPlayerLocal))
            return;
        LearnByDoing.GiveProgressionPoint(_targetEntity as EntityPlayerLocal, "perkInfiltrator");
    }
}

[HarmonyPatch(typeof(QuestEventManager))]
[HarmonyPatch("CraftedItem")]
public class arLearnByDoing_QuestEventManager_CraftedItem {
    private static FastTags foodTag = FastTags.Parse("food");
    public static void Postfix(ItemStack newStack) {
        if (!newStack.itemValue.ItemClass.ItemTags.Test_AllSet(foodTag))
            return;
        EntityPlayerLocal entityPlayer = XUiM_Player.GetPlayer() as EntityPlayerLocal;
        LearnByDoing.GiveProgressionPoint(entityPlayer, "perkMasterChef");
    }
}

[HarmonyPatch(typeof(QuestJournal))]
[HarmonyPatch("CompleteQuest")]
public class arLearnByDoing_QuestJournal_CompleteQuest {
    public static void Postfix(Quest q) {
        if (q.ID.StartsWith("quest_StarterClass"))
            return;
        EntityPlayerLocal entityPlayer = XUiM_Player.GetPlayer() as EntityPlayerLocal;
        LearnByDoing.GiveProgressionPoint(entityPlayer, "perkDaringAdventurer");
    }
}

[HarmonyPatch(typeof(EntityBuffs))]
[HarmonyPatch("SetCustomVar")]
public class arLearnByDoing_EntityBuffs_SetCustomVar {
    public static void Postfix(string _name, float _value, bool _netSync, EntityBuffs __instance) {
        if (!(__instance.parent is EntityPlayerLocal))
            return;
        if (_name != "foodHealthAmount")
            return;
        LearnByDoing.GiveProgressionPoint(__instance.parent as EntityPlayerLocal, "perkHealingFactor");
        
    }
}


/*
[HarmonyPatch(typeof(XUiC_RecipeStack))]
[HarmonyPatch("outputStack")]
public class arLearnByDoing_XUiC_RecipeStack_outputStack {
    public static void Postfix(bool __result) {
        Debug.Log("CRAFTED ITEM 2!");
    }
}
*/
/*
[HarmonyPatch(typeof(TileEntityWorkstation))]
[HarmonyPatch("HandleRecipeQueue")]
public class arLearnByDoing_TileEntityWorkstation_HandleRecipeQueue {
    public static bool Prefix(float _timePassed, bool ___bUserAccessing, bool[] ___isModuleUsed, bool ___isBurning, ItemStack[] ___output, RecipeQueueItem[] ___queue, TileEntityWorkstation __instance) {
        if (___bUserAccessing || ___queue.Length == 0 || ___isModuleUsed[3] && !___isBurning)
            return false;
        RecipeQueueItem recipeQueueItem = ___queue[___queue.Length - 1];
        if (recipeQueueItem == null)
            return false;
        if ((double) recipeQueueItem.CraftingTimeLeft >= 0.0)
            recipeQueueItem.CraftingTimeLeft -= _timePassed;
        while ((double) recipeQueueItem.CraftingTimeLeft < 0.0 && __instance.hasRecipeInQueue()) {
            if (recipeQueueItem.Multiplier > (short) 0) {
                ItemValue _itemValue = new ItemValue(recipeQueueItem.Recipe.itemValueType, false);
                if (ItemClass.list[recipeQueueItem.Recipe.itemValueType] != null && ItemClass.list[recipeQueueItem.Recipe.itemValueType].HasQuality)
                _itemValue = new ItemValue(recipeQueueItem.Recipe.itemValueType, (int) recipeQueueItem.Quality, (int) recipeQueueItem.Quality, false, (string[]) null, 1f);
                if (ItemStack.AddToItemStackArray(___output, new ItemStack(_itemValue, recipeQueueItem.Recipe.count), -1) == -1)
                break;
                --recipeQueueItem.Multiplier;
                recipeQueueItem.CraftingTimeLeft += recipeQueueItem.OneItemCraftTime;
                EntityPlayerLocal entityPlayer = XUiM_Player.GetPlayer() as EntityPlayerLocal;
                Debug.Log(entityPlayer.entityId + " " + recipeQueueItem.StartingEntityId);
                if (entityPlayer.entityId == recipeQueueItem.StartingEntityId) {
                    LearnByDoing.GivePlayerClassPoints(entityPlayer, "masterchef");
                }
            }
            if (recipeQueueItem.Multiplier <= (short) 0) {
                float craftingTimeLeft = recipeQueueItem.CraftingTimeLeft;
                __instance.cycleRecipeQueue();
                recipeQueueItem = ___queue[___queue.Length - 1];
                recipeQueueItem.CraftingTimeLeft += (double) craftingTimeLeft < 0.0 ? craftingTimeLeft : 0.0f;
            }
        }
        return false;
    }
}
*/

/*
[HarmonyPatch(typeof(Block))]
[HarmonyPatch("DamageBlock")]
public class arLearnByDoing_Block_DamageBlock {
    
    public static void Postfix(
        WorldBase _world,
        int _clrIdx,
        Vector3i _blockPos,
        BlockValue _blockValue,
        int _damagePoints,
        int _entityIdThatDamaged,
        bool _bUseHarvestTool,
        bool _bBypassMaxDamage,
        Block __instance
    ) {
        EntityPlayer _entity = XUiM_Player.GetPlayer();
        if (!((UnityEngine.Object) _entity != (UnityEngine.Object) null))
            return;
        if (_entity.entityId != _entityIdThatDamaged)
            return;
        if (!_bUseHarvestTool)
            return;
        float val = _entity.GetCVar("arlbdmining");
        //_entity.SetCVar("arlbdmining", val + 1f);
        Debug.Log(_entityIdThatDamaged + " " + _damagePoints);
        
    }
}

[HarmonyPatch(typeof(Progression))]
[HarmonyPatch("AddLevelExp")]
public class arLearnByDoing_GameUtils_collectHarvestedItem {
    public static bool Prefix(
        int _exp,
        string _cvarXPName,
        int __result,
        Progression __instance
    ) {
        Debug.Log(_cvarXPName + " => " + __result);
        return true;
    }
}

[HarmonyPatch(typeof(Block))]
[HarmonyPatch("PlaceBlock")]
public class arLearnByDoing_Block_PlaceBlock {
    
    public static void Postfix(
        WorldBase _world,
        BlockPlacement.Result _result,
        EntityAlive _ea,
        Block __instance
    ) {
        //Debug.Log(_entityIdThatDamaged + " " + _bUseHarvestTool);
    }
}

[HarmonyPatch(typeof(ItemActionEntryRepair))]
[HarmonyPatch("OnActivated")]
public class arLearnByDoing_ItemActionEntryRepair_OnActivated {
    
    public static void Postfix(
        ItemActionEntryRepair __instance
    ) {
        //Debug.Log(_entityIdThatDamaged + " " + _bUseHarvestTool);
    }
}
*/