using Harmony;
using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using DMT;
using Audio;

[HarmonyPatch(typeof(ItemClass))]
[HarmonyPatch("Init")]
public class AssortedRealism_ItemClass_Init {
    public static void Postfix(ItemClass __instance) {
        if (__instance.Properties.Values.ContainsKey("DegradationDropAfterBreak"))
            __instance.DegradationDropAfterBreak = __instance.Properties.Values["DegradationDropAfterBreak"];
    }
}

[HarmonyPatch(typeof(PlayerMoveController))]
[HarmonyPatch("OnGUI")]
public class AssortedRealism_PlayerMoveController_OnGUI {
    public static bool Prefix(PlayerMoveController __instance, ref GameManager ___gameManager, ref GUIWindowManager ___windowManager, ref EntityPlayerLocal ___entityPlayerLocal) {
        if (!___gameManager.gameStateManager.IsGameStarted() || GameStats.GetInt(EnumGameStats.GameState) != 1 || !___windowManager.IsHUDEnabled())
            return false;
        if (___entityPlayerLocal.inventory != null && ___gameManager.World.worldTime % 2UL == 0UL) {
            ItemValue holdingItemItemValue = ___entityPlayerLocal.inventory.holdingItemItemValue;
            ItemClass forId = ItemClass.GetForId(holdingItemItemValue.type);
            int maxUseTimes = holdingItemItemValue.MaxUseTimes;
            if (maxUseTimes > 0 && forId.MaxUseTimesBreaksAfter.Value && (double) holdingItemItemValue.UseTimes >= (double) maxUseTimes) {
                ___entityPlayerLocal.inventory.DecHoldingItem(1);
                ItemStack itemStack = new ItemStack(ItemClass.GetItem(forId.DegradationDropAfterBreak, false), 1);
                ___entityPlayerLocal.inventory.AddItem(itemStack);
                if (forId.Properties.Values.ContainsKey(ItemClass.PropSoundDestroy))
                    Manager.BroadcastPlay((Entity) ___entityPlayerLocal, forId.Properties.Values[ItemClass.PropSoundDestroy]);
            }
            ___entityPlayerLocal.equipment.CheckBreakUseItems();
        }
        if (___windowManager.IsInputActive() || ___windowManager.IsModalWindowOpen() || (UnityEngine.Event.current.rawType != UnityEngine.EventType.KeyDown || !___gameManager.IsEditMode()) || ___entityPlayerLocal.inventory == null)
            return false;
        ___gameManager.GetActiveBlockTool().CheckSpecialKeys(UnityEngine.Event.current, __instance.playerInput);
        return false;
    }
}