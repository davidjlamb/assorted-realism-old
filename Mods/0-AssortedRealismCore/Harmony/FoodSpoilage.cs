using Harmony;
using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using DMT;

[HarmonyPatch(typeof(ItemValue))]
[HarmonyPatch("Read")]
public class AssortedRealism_ItemValue_Read {
    public static void Postfix(BinaryReader _br, ItemValue __instance) {
        __instance.NextChangeTick = (int) _br.ReadUInt16();
    }
}

[HarmonyPatch(typeof(ItemValue))]
[HarmonyPatch("Write")]
public class AssortedRealism_ItemValue_Write {
    public static void Postfix(BinaryWriter _bw, ItemValue __instance) {
        _bw.Write((ushort) (__instance.NextChangeTick | 0));
    }
}

[HarmonyPatch(typeof(XUiC_ItemStack))]
[HarmonyPatch("Update")]
public class AssortedRealism_ItemStack_Update {
    public static void Postfix(XUiC_ItemStack __instance, bool ___bLocked, bool ___isDragAndDrop) {
        if(__instance.ItemStack.IsEmpty())
            return;
        if(__instance.ItemStack.itemValue == null)
            return;
        if(___bLocked && ___isDragAndDrop)
            return;
        if(__instance.ItemStack.itemValue.ItemClass == null || !__instance.ItemStack.itemValue.ItemClass.Properties.Contains("Spoilable"))
            return;
        float DegradationMax = 1000f;
        if(__instance.ItemStack.itemValue.ItemClass.Properties.Contains("SpoilageMax"))
            DegradationMax = __instance.ItemStack.itemValue.ItemClass.Properties.GetFloat("SpoilageMax");
        __instance.durability.IsVisible = true;
        __instance.durabilityBackground.IsVisible = true;
        float PerCent = 1f - Mathf.Clamp01(__instance.ItemStack.itemValue.UseTimes / DegradationMax);
        int TierColor = 7 + (int)Math.Round(8 * PerCent);
        if(TierColor < 0)
            TierColor = 0;
        if(TierColor > 7)
            TierColor = 7;
        __instance.durability.Color = QualityInfo.GetQualityColor(0);
        __instance.durability.Fill = PerCent;
    }
    public static bool Prefix(XUiC_ItemStack __instance, bool ___bLocked, bool ___isDragAndDrop) {
        if(__instance.ItemStack.IsEmpty())
            return true;
        if(__instance.ItemStack.itemValue == null)
            return true;
        if(___bLocked && ___isDragAndDrop)
            return true;
        if(__instance.ItemStack.itemValue.ItemClass == null || !__instance.ItemStack.itemValue.ItemClass.Properties.Contains("Spoilable"))
            return true;
        int TickPerLoss = 100;
        if(__instance.ItemStack.itemValue.ItemClass.Properties.Contains("SpoilageTicks"))
            TickPerLoss = __instance.ItemStack.itemValue.ItemClass.Properties.GetInt("SpoilageTicks");
        if(__instance.ItemStack.itemValue.NextChangeTick == 0)
            __instance.ItemStack.itemValue.NextChangeTick = (int)GameManager.Instance.World.GetWorldTime() + TickPerLoss;
        if(__instance.ItemStack.itemValue.NextChangeTick > (int)GameManager.Instance.World.GetWorldTime())
            return true;
        float DegradationMax = 0f;
        float DegradationPerUse = 0f;
        if(__instance.ItemStack.itemValue.ItemClass.Properties.Contains("SpoilageMax"))
            DegradationMax = __instance.ItemStack.itemValue.ItemClass.Properties.GetFloat("SpoilageMax");
        if(__instance.ItemStack.itemValue.ItemClass.Properties.Contains("SpoilagePerTick"))
            DegradationPerUse = __instance.ItemStack.itemValue.ItemClass.Properties.GetFloat("SpoilagePerTick");
        if (__instance.StackLocation == XUiC_ItemStack.StackLocationTypes.Creative)
            return true;
        if (__instance.StackLocation == XUiC_ItemStack.StackLocationTypes.ToolBelt)
            if(__instance.ItemStack.itemValue.ItemClass.Properties.Contains("SpoilageToolBelt"))
                DegradationPerUse += __instance.ItemStack.itemValue.ItemClass.Properties.GetFloat("SpoilageToolBelt");
            else
                DegradationPerUse += 6f;
        if (__instance.StackLocation == XUiC_ItemStack.StackLocationTypes.Backpack)
            if(__instance.ItemStack.itemValue.ItemClass.Properties.Contains("SpoilageBackpack"))
                DegradationPerUse += __instance.ItemStack.itemValue.ItemClass.Properties.GetFloat("SpoilageBackpack");
            else
                DegradationPerUse += 5f;
        if (__instance.StackLocation == XUiC_ItemStack.StackLocationTypes.LootContainer) {
            TileEntityLootContainer container = __instance.xui.lootContainer;
            if (container != null) {
                BlockValue Container = GameManager.Instance.World.GetBlock(container.ToWorldPos());
                if (Container.Block.Properties.Contains("PreserveBonus"))
                    DegradationPerUse -= Container.Block.Properties.GetFloat("PreserveBonus");
                else
                    DegradationPerUse += 10f;
            } else {
                if(__instance.ItemStack.itemValue.ItemClass.Properties.Contains("SpoilageContainer"))
                    DegradationPerUse += __instance.ItemStack.itemValue.ItemClass.Properties.GetFloat("SpoilageContainer");
                else
                    DegradationPerUse += 10f;
            }
        }
        float MinimumSpoilage = 0.1f;
        if (DegradationPerUse <= MinimumSpoilage)
            DegradationPerUse = MinimumSpoilage;
        int TotalSpoilageMultiplier = ((int)GameManager.Instance.World.GetWorldTime() - __instance.ItemStack.itemValue.NextChangeTick) / TickPerLoss;
        if (TotalSpoilageMultiplier == 0)
            TotalSpoilageMultiplier = 1;
        float TotalSpoilage = DegradationPerUse * TotalSpoilageMultiplier;
        __instance.ItemStack.itemValue.UseTimes += TotalSpoilage;
        __instance.ItemStack.itemValue.NextChangeTick = (int)GameManager.Instance.World.GetWorldTime() + TickPerLoss;
        while (DegradationMax <= __instance.ItemStack.itemValue.UseTimes) {
            String strSpoiledItem = "foodRottingFlesh";
            if (__instance.ItemStack.itemValue.ItemClass.Properties.Contains("SpoiledItem"))
                strSpoiledItem = __instance.ItemStack.itemValue.ItemClass.Properties.GetString("SpoiledItem");
            EntityPlayerLocal player = GameManager.Instance.World.GetPrimaryPlayer();
            if (player) {
                ItemStack itemStack = new ItemStack(ItemClass.GetItem(strSpoiledItem, false), 1);
                if(itemStack.itemValue.ItemClass.GetItemName() != __instance.ItemStack.itemValue.ItemClass.GetItemName())
                    if(!LocalPlayerUI.GetUIForPlayer(player as EntityPlayerLocal).xui.PlayerInventory.AddItem(itemStack, true))
                        player.world.gameManager.ItemDropServer(itemStack, player.GetPosition(), Vector3.zero, -1, 60f, false);
            }
            if(__instance.ItemStack.count > 2) {
                __instance.ItemStack.count--;
                __instance.ItemStack.itemValue.UseTimes -= DegradationMax;
            } else {
                __instance.ItemStack = new ItemStack(ItemValue.None.Clone(), 0);
                break;
            }
        }
        __instance.ForceRefreshItemStack();
        return true;
    }
}