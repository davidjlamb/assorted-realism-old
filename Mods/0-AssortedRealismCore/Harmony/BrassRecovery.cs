using Harmony;
using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using DMT;
using Audio;

[HarmonyPatch(typeof(ItemActionRanged))]
[HarmonyPatch("ConsumeAmmo")]
public class AssortedRealism_ItemActionRanged_ConsumeAmmo {
    public static void Postfix(ItemActionRanged __instance, ItemActionData _actionData) {
        if (!(_actionData.invData.holdingEntity is EntityPlayerLocal))
            return;
        ItemValue _ammo = ItemClass.GetItem(__instance.MagazineItemNames[(int) _actionData.invData.itemValue.SelectedAmmoTypeIndex], false);
        if (!_ammo.ItemClass.Properties.Values.ContainsKey("DropOnFire"))
            return;
        EntityPlayerLocal player = GameManager.Instance.World.GetPrimaryPlayer();
        ItemStack itemStack = new ItemStack(ItemClass.GetItem(_ammo.ItemClass.Properties.Values["DropOnFire"], false), 1);
        player.world.gameManager.ItemDropServer(itemStack, player.GetPosition(), Vector3.zero, -1, 60f, false);
    }
}