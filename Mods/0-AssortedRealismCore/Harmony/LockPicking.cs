using Harmony;
using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using DMT;
using Audio;

[HarmonyPatch(typeof(BlockSecureLoot))]
[HarmonyPatch("GetBlockActivationCommands")]
public class AssortedRealism_Lockpicking_GetBlockActivationCommands {
    public static void Postfix(BlockSecureLoot __instance, BlockActivationCommand[] __result, ref BlockActivationCommand[] ___cmds, ref string ___lockPickItem) {
        EntityPlayerLocal entityPlayer = XUiM_Player.GetPlayer() as EntityPlayerLocal;
        ___cmds[4].enabled = ___cmds[4].enabled && entityPlayer.inventory.holdingItemItemValue.ItemClass.GetItemName() == ___lockPickItem;
        __result = ___cmds;
    }
}

[HarmonyPatch(typeof(BlockSecureLoot))]
[HarmonyPatch("EventData_CloseEvent")]
public class AssortedRealism_Lockpicking_EventData_CloseEvent {
    public static bool Prefix(BlockSecureLoot __instance, TimerEventData timerData, ref float ___lockPickTime) {
        object[] data = (object[]) timerData.Data;
        Vector3i _blockPos = (Vector3i) data[2];
        EntityPlayerLocal entityPlayerLocal = data[3] as EntityPlayerLocal;
        ItemValue _itemValue = data[4] as ItemValue;
        Manager.BroadcastPlayByLocalPlayer(_blockPos.ToVector3() + Vector3.one * 0.5f, "Misc/locked");
        if (entityPlayerLocal.rand.RandomRange(0f, 1f) < 0.01f)
            entityPlayerLocal.PlayerUI.xui.PlayerInventory.Toolbelt.holdingItemItemValue.UseTimes = entityPlayerLocal.PlayerUI.xui.PlayerInventory.Toolbelt.holdingItemItemValue.MaxUseTimes;
        if (entityPlayerLocal.PlayerUI.xui.PlayerInventory.Toolbelt.holdingItemItemValue.MaxUseTimes > 0 && (double) entityPlayerLocal.PlayerUI.xui.PlayerInventory.Toolbelt.holdingItemItemValue.UseTimes >= (double) entityPlayerLocal.PlayerUI.xui.PlayerInventory.Toolbelt.holdingItemItemValue.MaxUseTimes) {
            if (entityPlayerLocal.PlayerUI.xui.PlayerInventory.Toolbelt.holdingItemItemValue.ItemClass.Properties.Values.ContainsKey(ItemClass.PropSoundDestroy))
                Manager.PlayInsidePlayerHead(entityPlayerLocal.PlayerUI.xui.PlayerInventory.Toolbelt.holdingItemItemValue.ItemClass.Properties.Values[ItemClass.PropSoundDestroy], -1, 0.0f, false, false);
                entityPlayerLocal.PlayerUI.xui.PlayerInventory.Toolbelt.DecHoldingItem(1);
        } else {
            entityPlayerLocal.PlayerUI.xui.PlayerInventory.Toolbelt.holdingItemItemValue.UseTimes += EffectManager.GetValue(PassiveEffects.DegradationPerUse, entityPlayerLocal.PlayerUI.xui.PlayerInventory.Toolbelt.holdingItemItemValue, 1f, entityPlayerLocal, (Recipe) null, entityPlayerLocal.PlayerUI.xui.PlayerInventory.Toolbelt.holdingItemItemValue.ItemClass.ItemTags, true, true, true, true, 1, true);
        }
        entityPlayerLocal.PlayerUI.xui.PlayerInventory.Toolbelt.ForceHoldingItemUpdate();
        TileEntitySecureLootContainer tileEntity = GameManager.Instance.World.GetTileEntity((int) data[0], _blockPos) as TileEntitySecureLootContainer;
        if (tileEntity == null)
            return false;
        tileEntity.PickTimeLeft = Mathf.Max(___lockPickTime * 0.25f, timerData.timeLeft);
        __instance.ResetEventData(timerData);
        return false;
    }
}

[HarmonyPatch(typeof(BlockSecureLoot))]
[HarmonyPatch("EventData_Event")]
public class AssortedRealism_Lockpicking_EventData_Event {
    public static bool Prefix(BlockSecureLoot __instance, TimerEventData timerData) {
        World world = GameManager.Instance.World;
        object[] data = (object[]) timerData.Data;
        int _clrIdx = (int) data[0];
        BlockValue blockValue = (BlockValue) data[1];
        Vector3i vector3i = (Vector3i) data[2];
        BlockValue block = world.GetBlock(vector3i);
        EntityPlayerLocal entityPlayerLocal = data[3] as EntityPlayerLocal;
        object obj = data[4];
        if ((world.GetTileEntity(_clrIdx, vector3i) as TileEntitySecureLootContainer).IsUserAccessing()) {
            GameManager.ShowTooltipWithAlert(entityPlayerLocal, Localization.Get("ttCantPickupInUse", ""), "ui_denied");
        } else {
            if (entityPlayerLocal.rand.RandomRange(0f, 1f) < 0.01f)
                entityPlayerLocal.PlayerUI.xui.PlayerInventory.Toolbelt.holdingItemItemValue.UseTimes = entityPlayerLocal.PlayerUI.xui.PlayerInventory.Toolbelt.holdingItemItemValue.MaxUseTimes;
            if (entityPlayerLocal.PlayerUI.xui.PlayerInventory.Toolbelt.holdingItemItemValue.MaxUseTimes > 0 && (double) entityPlayerLocal.PlayerUI.xui.PlayerInventory.Toolbelt.holdingItemItemValue.UseTimes >= (double) entityPlayerLocal.PlayerUI.xui.PlayerInventory.Toolbelt.holdingItemItemValue.MaxUseTimes) {
                if (entityPlayerLocal.PlayerUI.xui.PlayerInventory.Toolbelt.holdingItemItemValue.ItemClass.Properties.Values.ContainsKey(ItemClass.PropSoundDestroy))
                    Manager.PlayInsidePlayerHead(entityPlayerLocal.PlayerUI.xui.PlayerInventory.Toolbelt.holdingItemItemValue.ItemClass.Properties.Values[ItemClass.PropSoundDestroy], -1, 0.0f, false, false);
                    entityPlayerLocal.PlayerUI.xui.PlayerInventory.Toolbelt.DecHoldingItem(1);
            } else {
                entityPlayerLocal.PlayerUI.xui.PlayerInventory.Toolbelt.holdingItemItemValue.UseTimes += EffectManager.GetValue(PassiveEffects.DegradationPerUse, entityPlayerLocal.PlayerUI.xui.PlayerInventory.Toolbelt.holdingItemItemValue, 1f, entityPlayerLocal, (Recipe) null, entityPlayerLocal.PlayerUI.xui.PlayerInventory.Toolbelt.holdingItemItemValue.ItemClass.ItemTags, true, true, true, true, 1, true);
            }
            entityPlayerLocal.PlayerUI.xui.PlayerInventory.Toolbelt.ForceHoldingItemUpdate();
            LocalPlayerUI.GetUIForPlayer(entityPlayerLocal);
            if (__instance.DowngradeBlock.type != BlockValue.Air.type) {
                BlockValue downgradeBlock = __instance.DowngradeBlock;
                BlockValue _blockValue = BlockPlaceholderMap.Instance.Replace(downgradeBlock, world.GetGameRandom(), vector3i.x, vector3i.z, false, QuestTags.none);
                _blockValue.rotation = block.rotation;
                _blockValue.meta = block.meta;
                world.SetBlockRPC(_clrIdx, vector3i, _blockValue, Block.list[_blockValue.type].Density);
            }
            Manager.BroadcastPlayByLocalPlayer(vector3i.ToVector3() + Vector3.one * 0.5f, "Misc/unlocking");
            __instance.ResetEventData(timerData);
        }
        return false;
    }
}